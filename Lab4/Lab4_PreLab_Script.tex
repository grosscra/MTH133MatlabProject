\documentclass[12pt]{article}
\usepackage{fancyhdr}
\usepackage{geometry}
\usepackage{graphicx}
\usepackage{enumitem}
\usepackage{amsmath}

\geometry{letterpaper, scale=0.80, hmarginratio={1:1}, vmarginratio={1:1}, headheight=20pt, headsep=0.2in, footskip=0.5in}

\pagestyle{fancy}
\fancyhf{}
\rhead{July 3, 2019}
\lhead{MTH133}
\chead{\large \bf Lab 4 Pre-Lab Script}
\cfoot{\thepage}

\setlength\parindent{0pt}
\setlength\parskip{15pt}

\begin{document}

\section*{Introduction}

\begin{itemize}
	\item Discuss learning goals
		\begin{itemize}
			\item Welcome to the Lab 4 Pre-Lab where we will be dealing with the idea of quantization.
			\item This means that we'll be breaking down a real-life phenomenon into discrete chunks that we can analyze using techniques from calculus II.
			\item In particular, we'll see how geometric sequences show up when controlling measurement errors from this process of analog to digital conversion of scans of Wells Hall.
		\end{itemize}
	\item Problem setup
		\begin{itemize}
			\item But what are we scanning for?
			\item Well Wells Hall is experiencing a tribble problem.
			\item \emph{(Show cartoons of the concepts discussed, placing a drawing of a tribble into a drawing of Wells Hall.)}
			\item If you haven't seen a tribble before, they're rapidly reproducing fuzzballs which have the tendency to overrun their surroundings if not properly managed.
			\item This one has worked its way into the walls of the long hallway stretching across Wells Hall.
			\item We'll say that the tribble is one foot long in the 800 foot long hallway.
			\item Now, we'll be taking care of our problem with a single-use device that can neuter anything within a one foot range.
				Our goal is to figure out which one foot length of the hallway to apply this to.
			\item In order to do this, we have a technician running a scanner.
				We give him a section of the hallway to scan, and he tells us whether the tribble is or is not in that section.
			\item And we give him this scan as an interval: to scan the distance from the 400th foot to the 500th foot, we'll give him the interval $[400, 500]$.
			\item These scans are expensive to do, so we want to use as few as possible.
			\item If you'd like to give it a shot, there is a game programmed in the \texttt{MATLAB} live-script which allows you to experience the scanning process.
		\end{itemize}
\end{itemize}

\section*{Example and binary search explanation}

\begin{itemize}
	\item So let's go through the best way to do these scans.
		The main idea is to cut down the amount of uncertainty we have of the tribble's position as much as we can after each scan.
	\item \emph{(Have the supplemental document open with the tribble position marked to run through the example. As the scans come back, we'll highlight pieces of the hallway.)}
	\item Start example
		\begin{itemize}
			\item At the start, we're going to run the scanner over half of the hallway.
				This means we'll give the technician the interval $[0, 400]$ to scan.
			\item We get one of two possibilities:
				\begin{itemize}
					\item The tribble is in the range, or
					\item The tribble is not in the range.
						In this case then, we know for certain that the tribble must be in the other half of the interval, from $[400, 800]$.
				\end{itemize}
			\item Either way, we know that the tribble is now within a $400$ foot range of the hall.
				So we cut down our uncertainty of the tribble's position by half, which is the best we can do.
				Try to think about the worst case scenario if we had scanned, say, the interval $[0, 200]$ to see why this makes sense.
			\item In this example, the scan comes back positive, so we know it's in the interval $[0, 400]$.
			\item Since we've narrowed it down to here, let's try scanning again.
			\item Again, we can cut our uncertainty in half by scanning the first half of this interval $[0, 200]$.
			\item For this example, it comes up negative, so we know that the tribble must be in the other half $[200, 400]$.
			\item We would continue by scanning $[200, 300]$, and so on...
		\end{itemize}
	\item Formalization
	\item \emph{(Have an arbitrary interval, ready to be labeled and split up.)}
	\begin{itemize}
		\item Now let's try pin down exactly what's happening at each step.
		\item Say we have an interval $[a, a + L]$ that we know the tribble is in.
		\item Our tactic is to always scan the first half of the interval.
			In this case, it will be $[a, a + L/2]$.
		\item Either it comes back positive or negative.
			\begin{itemize}
				\item A positive scan narrows us down to that interval $[a, a + L/2]$.
					When we scan again, we scan the first half of this, $[a, a + L/4]$.
				\item A negative scan narrows it to the other half $[a + L/2, a + L]$.
					When we scan again, we scan the first half of this, $[a + L/2, a + L/2 + L/4]$.
			\end{itemize}
		\item But notice that we've trapped the tribble into an interval that is length $L/2$ no matter what.
	\end{itemize}
	\item Analysis
		\begin{itemize}
			\item If we try to analyze this mathematically, we can realize that by cutting down the uncertainty by half each time, this is like creating a geometric sequence.
			\item \emph{(Write out the following steps.)}
			\[
				800 \rightarrow 400 \rightarrow 200 \rightarrow 100 \rightarrow 50 \rightarrow \ldots \rightarrow 800 / (2^n) \rightarrow \ldots
			\]
			\item So after the $n$th scan, our uncertainty is $800 / (2^n)$.
			\item Now, what does it mean to pin down the position of the tribble?
				This is when our uncertainty is less than the length of the hall it could be hiding in.
			\item Mathematically, this just means that the uncertainty $800 / (2^n)$ is less than $1$.
			\item If we plug in some values for $n$, we find out that the first scan that this happens for is $10$, since $2^{10}$ is 1024.
			\item So this guarantees that the first scan that we'll find the tribble on is the 10th scan, every time.
		\end{itemize}
\end{itemize}

\section*{Introducing the faulty lens}

\begin{itemize}
	\item So we're able to come up with an efficient strategy for this situation, which we're able to analyze using geometric series.
	\item But in the actual lab, we will be dealing with something a bit more complex.
	\item The technician tells us that now there's a problem with the zoom lens on the scanner.
	\item \emph{(Show visualization of margin of error.)}
	\item This means that when we scan a section of the hallway, there is a fuzzy region around the scanned interval.
	\item In this fuzzy region, it's possible to get a false positive, that is, the scanner will tell us the tribble is there, when it's actually not.
	\item And this fuzzy region is half the length of the region we scanned on either side of the scanned interval.
	\item So let's take an example:
		\begin{itemize}
			\item If we scan the interval $[100, 250]$, length of this false positive region will be $75$, half of the length of the interval.
			\item So if the tribble is in the interval, the scanner will say that the tribble is there.
			\item And if the tribble is outside the interval \emph{and} the fuzzy region, the scanner will say the correct thing, that the tribble is not in the scanned interval.
			\item However, if the tribble is in the fuzzy region, either from $[25, 100]$ or $[250, 325]$, the scanner may give us a false positive.
				It might correctly say that the tribble is not in the scanned interval, but it might also incorrectly say that the tribble is in the scanned interval.
		\end{itemize}
	\item So your task in the lab is to figure out whether we can adapt our binary search strategy to this situation, and figure out how we can most effectively find the tribble.
\end{itemize}

\end{document}
