\documentclass[12pt]{article}
\usepackage{fancyhdr}
\usepackage{geometry}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{enumitem}

\geometry{letterpaper, scale=0.80, hmarginratio={1:1}, vmarginratio={1:1}, headheight=20pt, headsep=0.2in, footskip=0.5in}

\pagestyle{fancy}
\fancyhf{}
\rhead{Feb. 26, 2019}
\lhead{MTH133}
\chead{\large \bf Lab 4: Pre-Lab}
\cfoot{\thepage}

\setlength\parindent{0pt}
\setlength\parskip{15pt}

\definecolor{grayBG}{gray}{0.85}
\newcommand\answerbox[1]{\raisebox{-0.8ex}{\textcolor{grayBG}{\rule{#1}{3ex}}}}

\begin{document}

\section*{Learning Goals}

Today you will explore the concept of \textbf{quantization}, the conversion of analog signals to digital ones.
Pretty much any device that takes a measurement and provides a digital output (smart watches and digital bathroom scales just to name two) has a built-in quantization algorithm.
This lab will focus on one implementation, which relies heavily on the concept of \textbf{geometric sequence/series}.

\section*{Tribbles!}

Tribbles have infested the walls of Wells Hall.
Well, one tribble so far has been sighted.
But these small, furry, gentile, and slow-moving creatures reproduce \emph{very} rapidly.
In a couple of days, we expect to be overrun.

Your mission: find the tribble, and neutralize the cuddly threat.

\section*{Wells Hall}

We know that the tribble is hiding along the main hallway in Wells Hall on the ground floor pictured in Figure \ref{fig:WellsHall} .
The hallway has a total length of (approximately) \textbf{800 feet}.

For convenience we will measure our distances from the \emph{A wing} end of the building.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=0.6\linewidth]{Figures/WellsHall.pdf}
	\caption{Wells Hall}
	\label{fig:WellsHall}
\end{figure}

\section*{Your tools}

Unfortunately, due to budget cuts, our resources are limited.

To neutralize the tribble, you have a single-use device that can neuter any living thing within a \textbf{1 foot long} section of the hallway.

To locate the tribble, we have a remote sensor located across campus in the bug house in the CNS building.
You can radio in a section of Wells Hall you want scanned and our technician there will tell you whether the tribble is within that section of wall.
It's expensive to take scans.
\textbf{You should try to scan as few times as possible}.

To get a feel for the scanning process, there is a game programmed in the \texttt{MATLAB} lab document simulating the search.
Try out a few scans, and then read on to learn the optimal scanning strategy.

\section*{Mathematical Explanation}

The standard strategy for these kinds of games is that of \textbf{binary search}.

At each step, the interval you choose to scan should be exactly \textbf{half} of what you don't know about; this way the uncertainty of where the tribble is decreases by half at each step: the tribble is either in the half that you scanned or the half that you didn't scan.

Let's go through an example to clarify this strategy:

\begin{enumerate}
	\item[0.] To start, all we know is that the tribble is somewhere in the 800 foot hallway.
		We represent this numerically by the interval $[0, 800]$.
	\item We scan half of the length of the hallway, the interval $[0, 400]$.
		The technician tells us that the tribble \emph{is} in this range.
		So we cross out the other half of the hallway, the interval $[400, 800]$.

		\begin{figure}[ht!]
			\centering
			\includegraphics[width=0.75\textwidth]{Figures/1.pdf}
		\end{figure}

		Notice that our uncertainty in the tribble's position has now reduced by half.
		We started out knowing that the tribble's position could be anywhere in 800 feet of hallway, and by taking one scan, we now know that the tribble is confined to only 400 feet of hallway.

	\item We now scan half of the remaining interval.
		We'll scan the first half $[0, 200]$.
		The technician now tells us that the tribble \emph{is not} in this range.
		So we cross out this piece of the hallway.

		\begin{figure}[ht!]
			\centering
			\includegraphics[width=0.75\textwidth]{Figures/2.pdf}
		\end{figure}

		However, this again decreases our uncertainty by half.
		Even though the tribble wasn't in the interval we scanned, we know that it \emph{must be} in the other half, that is, the interval $[200, 400]$.
	
	\item We keep going, scanning half of the remaining interval $[200, 400]$, and so on\ldots At the end of this document, we complete this example. But for now, let's jump to what happens in the $N$th step.
	\item[N.] Let's say that from the previous step, we know that the tribble is in an interval of length $L$ starting at $a$.
		We can write this interval as $[a, a + L]$.
		We'll choose to scan the first half of this interval, $[a, a + L/2]$.
		Depending on the result of the scan, we have two options:
		\begin{enumerate}
			\item The tribble is in the scanned interval.
				We proceed by scanning half of that interval, $[a, a + L/4]$.
			\item The tribble is not in the scanned interval.
				We know then that it must be in the other half, the interval $[a + L/2, a + L]$.
				We proceed by scanning half of that interval, $[a + L/2, a + L/2 + L/4]$.
		\end{enumerate}
		Either way, we've now narrowed the tribble down to an interval of half the length we knew before.

\end{enumerate}

Notice that the \textbf{level of uncertainty} you have forms a \textbf{geometric sequence} since it is cut down by half after each scan: 

\begin{table}[ht!]
	\centering
	\begin{tabular}{ccccccccc}
	 Scan & $0$ & $1$ & $2$ & $3$ & $4$ & $\cdots$ & $n$ & $\cdots$ \\ \hline
	 Uncertainty & $800$ & $400$ & $200$ & $100$ & $50$ & $\cdots$ & $800 / (2^n)$ & $\cdots$
	\end{tabular}
\end{table}

We end the scanning process once we've narrowed down our uncertainty to just 1 foot of the hallway.
This level of precision is achieved after the $n$th step, when
\[
	800 / (2^n) \leq 1.
\]
We look for the smallest $n$ possible, to minimize the number of scans.
This is the same thing as requiring that $800 \leq 2^n$, and after plugging in a few values of $n$, we see that 
\[
	2^9 = 512 < 800 < 2^{10} = 1024.
\]
So after 10 scans we guarantee for the first time that we our uncertainty is reduced to the 1 foot range of containing the tribble.

\section*{The real enchilada}

After you've successfully scanned found the tribble using the binary search strategy, the technician hurries you out of the door to confront the tribble. 

Before he lets you go, however, he tells you:
``Oh, by the way, we have a small problem with our scanner.
It sometimes gives false positives when the tribble is close to the end points of the intervals being scanned.
This means that if the scanner doesn't detect the tribble, there definitely isn't one there.
But if the scanner does detect the tribble, there's a chance that it's wrong. 
The tribble either could be in the interval scanned or just be \emph{close} to the interval scanned.''

``How big is the error?'' you ask.

``About 50\% the length being scanned. Look, let me give you an example. Say you tell me to scan the interval $[100,250]$, right? This interval is 150 ft long, so there's a fuzzy area of length 75 ft on either end.

So here are the possibilities of the scan:

\begin{enumerate}
	\item If the tribble is actually in the scanned interval $[100,250]$:

	\begin{figure}[ht!]
		\centering
		\includegraphics[scale=0.8]{Figures/4b-1.pdf}
	\end{figure}

	The machine will always tell you the right answer:
	\[
		\texttt{Scan result: Tribble is in range scanned [100,250]}
	\]

	\item If the tribble is more than 75 feet away from the endpoints of the interval being scanned:

	\begin{figure}[ht!]
		\centering
		\includegraphics[scale=0.8]{Figures/4b-2.pdf}
	\end{figure}
	
	The machine will always tell you the right answer:
	\[
		\texttt{Scan result: Tribble is not in range scanned [100,250]
}	\]
	
	\item  But if the tribble is between $[25,100]$ or $[250,325]$?
	
	\begin{figure}[ht!]
		\centering
		\includegraphics[scale=0.8]{Figures/new-3.pdf}
	\end{figure}
		
	Well, the machine sometimes gets it right, and sometimes gets it wrong.
	You'll either see the correct answer
	\[
		\texttt{Scan result: Tribble is not in range scanned [100,250]
}	\]
	or the wrong answer
	\[
		\texttt{Scan result: Tribble is in range scanned [100,250]
}	\]
	but you won't know ahead of time.''
	
\end{enumerate}

``It has something to do with our zoom lens.
So the problem only happens if the tribble is not in the interval your specified,
but within half of the interval length to one of the end points.''

In the lab you will analyze this new setup and come up with a strategy to find the tribble, even with the faulty lens.

% We can formalize the strategy in the following Step-by-Step guide. 
% 
% \subsection*{Step 1}
%  
% Start by scanning the interval $[0,400]$.
% 
% \subsection*{Step 2}
% 
%  Based on the technician's response you do either:
% 
% \begin{enumerate}
% 	\item If the technician tells you that the tribble is in $[0,400]$, you now know that the tribble is in the interval $[0,400]$, and you next scan $[0,200]$. 
% 	\item If the technician tells you that the tribble is not, you now know that the tribble is in the interval $[400,800]$, and you next scan $[400,600]$.
% \end{enumerate}
% 
% Notice that in either case you scan the "left half" of the interval in which the presence of the tribble hasn't yet been ruled out. 
% 
% \subsection*{Step N}
% 
% Suppose at the end of the previous step, based on the technician's answers, you know that the tribble is in an interval $[a,a+L]$ of length L.
% 
% The binary search method will tell you to scan the interval $[a, a + L/2]$. After which,
% 
% \begin{enumerate}
% 	\item  If the technician tells you that the tribble is in your scanned interval, you next scan $[a, a+ L/4]$.
% 	\item  If the technician tells you that the tribble is not in your scanned interval, you next scan $[a+L/2, a+L/2+L/4]$.
% \end{enumerate}
% 	
% Notice that the \textbf{level of uncertainty} you have forms a \textbf{geometric sequence}:
% \[
% 	800, 400, 200, 100, 50, ... , 800/(2^n), ... 
% \]
% 
% and the number of scans required is the smallest $n$ such that
% 
% \[
% 	800/(2^n) <= 1
% \]
% 
% Using that \(2^9 = 512 < 800 < 2^{10} = 1024\), we see that 10 scans are required to conclusively pin-point the position of the tribble within a section that is at most 1 ft wide. 

\section*{Finishing the binary search example}

Starting at step 3:

\begin{enumerate}
	\item[3.] Scan result: Tribble is in range scanned $[200, 300]$.
		\begin{figure}[ht!]
			\centering
			\includegraphics[width=0.75\textwidth]{Figures/3.pdf}
		\end{figure}
	\item[4.] Scan result: Tribble is NOT in range scanned $[200, 250]$.
		\begin{figure}[ht!]
			\centering
			\includegraphics[width=0.75\textwidth]{Figures/4.pdf}
		\end{figure}
	\item[5.] Scan result: Tribble is NOT in range scanned $[250, 275]$.
		\begin{figure}[ht!]
			\centering
			\includegraphics[width=0.75\textwidth]{Figures/5.pdf}
		\end{figure}
	\item[6.] Scan result: Tribble is in range scanned $[275, 287]$.
		Notice that at this point, we were supposed to scan an interval of length $12.5$, but we can only plug whole numbers into the scanner.
		\begin{figure}[ht!]
			\centering
			\includegraphics[width=0.75\textwidth]{Figures/6.pdf}
		\end{figure}
		So instead we just round down at each opportunity.
	\item[7.] Scan result: Tribble is in range scanned $[275, 281]$.
		\begin{figure}[ht!]
			\centering
			\includegraphics[width=0.75\textwidth]{Figures/7.pdf}
		\end{figure}
	\item[8.] Scan result: Tribble is in range scanned $[275, 278]$.
		\begin{figure}[ht!]
			\centering
			\includegraphics[width=0.75\textwidth]{Figures/8.pdf}
		\end{figure}
	\item[9.] Scan result: Tribble is NOT in range scanned $[275, 276]$.
		\begin{figure}[ht!]
			\centering
			\includegraphics[width=0.75\textwidth]{Figures/9.pdf}
		\end{figure}
	\item[10.] Scan result: Tribble is NOT in range scanned $[276, 277]$.
		\begin{figure}[ht!]
			\centering
			\includegraphics[width=0.75\textwidth]{Figures/10.pdf}
		\end{figure}
\end{enumerate}
So the tribble must be in the only interval left: $[277, 278]$.

% We have done an example scanning process using the code provided in the warm up round of the lab file, and the supplementary materials.
% 
% After 10 scans, this is what we have got. 
% 
% 1. ScanResult = Tribble is in range scanned $[0, 400]$
% 
% \includegraphics[scale = 0.75]{Figures/Slide1.JPG}
% 
% 2. ScanResult = Tribble is in range scanned $[0, 200]$
% 
% \includegraphics[scale = 0.75]{Figures/Slide2.JPG}
% 
% 3. ScanResult = Tribble is in range scanned $[0, 100]$
% 
% \includegraphics[scale = 0.75]{Figures/Slide3.JPG}
% 
% 4. ScanResult = Tribble is in range scanned $[0, 50]$
% 
% \includegraphics[scale = 0.75]{Figures/Slide4.JPG}
% 
% 5. ScanResult = Tribble is in range scanned $[0, 25]$
% 
% \includegraphics[scale = 0.75]{Figures/Slide5.JPG}
% 
% 6. ScanResult = Tribble is in range scanned $[0, 12]$
% 
% \includegraphics[scale = 0.75]{Figures/Slide6.JPG}
% 
% 7. ScanResult = Tribble is in range scanned $[0, 6]$
% 
% \includegraphics[scale = 0.75]{Figures/Slide7.JPG}
% 
% 8. ScanResult = Tribble is in range scanned $[0, 3]$
% 
% \includegraphics[scale = 0.75]{Figures/Slide8.JPG}
% 
% 9. ScanResult = Tribble is in range scanned $[0, 2]$
% 
% \includegraphics[scale = 0.75]{Figures/Slide9.JPG}
% 
% 10. ScanResult = Tribble is in range scanned $[0, 1]$
% 
% \includegraphics[scale = 0.75]{Figures/Slide10.JPG}
% 
% As you can see, the resulting scan is within 1 ft wide as mentioned previously.


\end{document}
