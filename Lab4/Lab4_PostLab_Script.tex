\documentclass[12pt]{article}
\usepackage{fancyhdr}
\usepackage{geometry}
\usepackage{graphicx}
\usepackage{enumitem}
\usepackage{amsmath}

\geometry{letterpaper, scale=0.80, hmarginratio={1:1}, vmarginratio={1:1}, headheight=20pt, headsep=0.2in, footskip=0.5in}

\pagestyle{fancy}
\fancyhf{}
\rhead{June 13, 2019}
\lhead{MTH133}
\chead{\large \bf Lab 4 Post-Lab Script}
\cfoot{\thepage}

\setlength\parindent{0pt}
\setlength\parskip{15pt}

\begin{document}

\section*{Exercise 1}

\begin{itemize}
	\item Intro:
	\begin{itemize}
		\item Welcome to the post-lab videos for Lab 4.
		\item Today we'll be scanning Wells Hall for tribbles.
	\end{itemize}
	\item Model:
	\begin{itemize}
		\item The scanner works by giving an interval, and then receiving a positive result if the tribble is in that interval, and a negative result if it's not.
		\item But remember that in the main part of this lab, we're scanning with a faulty zoom lens.
		\begin{itemize}
			\item This means that when we scan some length of the hall looking for a tribble, we have a 50\% margin of error.
			\item So if the tribble is inside the interval we scanned, we'll get a positive result.
			\item If the tribble is outside where we scanned AND is farther outside the interval by 50\% of the interval's length, we will get a negative result.
			\item But for tribbles within 50\% of the interval, it's possible to obtain false positives.
		\end{itemize}
	\end{itemize}
	\item This faulty zoom lens can be pretty problematic for us.
	\item Remember that when the scanner is fully functional, we can just scan half of the interval.
	\begin{itemize}
		\item If the result is positive, we know that the tribble is in that half.
			We can then scan half of that interval.
		\item If the result is negative, we know that the tribble is in the other half, and then we can go to scanning that half of the interval.
	\end{itemize}
	\item But let's look at an example where the faulty zoom lens messes up this process.
	\begin{itemize}
		\item Let's try and start by coming up with a situation where we get a false positive.
			Then from there, we can see if this can force us into the wrong solution.
		\item So if we first scan the interval $[0, 400]$, like before, let's pretend the tribble is in the error region, within $200$ feet of this interval.
			For example, let's put the tribble kind of far out, right at $600$ feet.
		\item Now, we'll say that the faulty zoom lens picks this up as a false positive.
			So if we just keep going with our algorithm from before, we'll now assume that the tribble is in $[0, 400]$!
		\item This is hopeless for us.
			If we keep scanning halves of this region, we'll always pick up false.
			So we'll keep thinking it's in the other half of whatever section we scanned, and we'll eventually come to the wrong position.
		\item Right, next we'll try $[0, 200]$, which will come back negative, so we'll assume that the tribble is in $[200, 400]$, when it definitely isn't!
		\item Then we'll try $[200, 300]$ which will be negative making us think it's in $[300, 400]$, and eventually we'll incorrectly conclude that the tribble was at $400$ feet down the hallway.
	\end{itemize}
	\item So the moral of the story is that we need a better scanning strategy.
		This will be the focus of our next video, so see you then.
\end{itemize}

\section*{Exercise 2, Part 1}

\begin{itemize}
	\item Alright, now we're going to try to get an algorithm together which ensures that we find the tribble, even with the faulty zoom lens.
	\item Then, just like the binary search, we'll analyze our algorithm to figure out how many scan we actually need.
	\item So let's assume we follow the process we had before: we first scan $[0, 400]$.
	\item This will either come back positive or negative:
		\begin{itemize}
			\item We know for sure that if it's negative, the tribble isn't there.
				So we can put the tribble down as being in $[400, 800]$, and we can scan away like usual, taking half of that interval.
				So our next scan will be $[400, 600]$.
			\item On the other hand, if we get a positive result, well there are two possibilities:
				\begin{itemize}
					\item One is that the tribble actually is in the interval we scanned $[0, 400]$.
					\item The other is that the tribble is in the ``error'' interval, which stretches out 50\% from the search interval.
						This means that it will be $200$ feet long, and so will cover $[400, 600]$.
				\end{itemize}
				So all we can conclude is that the tribble is in $[0, 600]$.
			\item From there, we can then follow our same philosophy and try scanning half that interval, so we'll scan $[0, 300]$, and start it all over again.
			\item Notice that even though we don't know that our tribble is exactly where we search, we're still narrowing down the window a little bit, and finding some (possibly larger than we'd like) interval where the tribble is \emph{guaranteed to be}.
		\end{itemize}
	\item Now let's go onto this third part, where we do this same thing, but just a little more general.
	\item So we assume we've narrowed down the tribble's location to an interval starting at some point $a$ which is length $L$ long.
		We write this as $[a, a + L]$.
	\item As we've done before, we'll scan the first half of that interval $[a, a + L/2]$, and based on the results, we'll decide how to proceed.
	\item Like we said before, if the result is negative, the tribble definitely isn't there, so we know it's in the other half interval $[a + L/2, a + L]$.
	\item If the result is positive, again, it's either in the interval or the ``error interval''.
	\item Remember that the error interval is 50\% the length of the just scanned interval, so half of $L/2$ which is $L/4$.
		So the error interval stretches from $a + L/2$ to $a + L/2 + L/4$.
	\item This means we know that the tribble is in the interval from $a$ to $a + L/2 + L/4 = a + 3L/4$.
	\item Then we'll scan half that and keep going, so the new interval is $[a, a + 3L/8]$.
	\item So this defines our algorithm for finding the tribble.
		We can follow these rules until we pin down the exact location of the tribble.
	\item In the next video, we'll analyze how this algorithm works, and see just how many scans we need to make.
\end{itemize}

\section*{Exercise 2, Part 2}
\begin{itemize}
	\item Okay, we have our algorithm, now let's figure out how it works.
	\item So we want to see the ``worst case'' situation.
	\item If we compare the two cases of getting a positive and a negative result, we see that the length of the interval we can narrow our search down to is larger for the positive result rather than the negative result.
	\item This means that when we get positive results, there's more uncertainty at each step than when we get negative results.
	\item So let's assume we get positive results every time.
	\item Now before scanning, we have $800$ feet to work with.
	\item A positive first scan narrows it down to the first $600$ feet we said.
	\item Then if we scan the first half of that, the interval $[0, 300]$ and get a positive result, well, the length of the interval was $600$, it has to be in the first ``three times six hundred over four'' feet of that interval like we said above.
		This is the first half plus the error interval.
	\item This comes out to the first $450$ feet of the hall.
	\item If we scan the first half again and get a positive result, then again we know it's in the first ``three times four fifty over four'' feet of the hall which is about $338$, and so on.
	\item If we put this in terms of our previous steps, we know
		\begin{align*}
			600 &= 800 \cdot \left( \frac{3}{4} \right) \\
			450 &= 800 \cdot \left( \frac{3}{4} \right) \cdot \left( \frac{3}{4} \right) \\
				&= 800 \cdot \left( \frac{3}{4} \right)^2 \\
			338 &\approx 800 \cdot \left( \frac{3}{4} \right)^2 \cdot \left( \frac{3}{4} \right) \\
				&= 800 \cdot \left( \frac{3}{4} \right)^3 \\
				&\vdots
		\end{align*}
	\item So our uncertainty after our $n$th scan is $800 \cdot \left( \frac{3}{4} \right)^n$.
	\item Finally, we want to use this to figure out the required number of scans.
	\item Well, we know to stop once we've narrowed our uncertainty down to the last possible foot, so we need our uncertainty to be less than $1$.
		This means we just have to find $n$ so that
		\[
			800 \cdot \left( \frac{3}{4} \right)^n < 1.
		\]
	\item We can solve this equation by moving the fraction to the other side and taking the log of both sides:
		\begin{align*}
			& 800 < \left( \frac{4}{3} \right)^n \\
			\implies & \log_{4/3} 800 < n \\
			\implies & 23.3 < n.
		\end{align*}
	\item So if $n$ is $24$ which is just bigger than $23.3$, then we our uncertainty will be narrowed down to just one foot in the worst case scenario.
	\item But of course remember that this is just in the worst-case scenario.
		If we ever hit a negative result anywhere, we actually knock off half the size of the interval rather than $3/4$.
		So it could go faster than this!
		In fact, if we got a negative result, reducing our uncertainty by half each time, this is the same type of procedure covered in the pre-lab.
		In the best-case scenario then, it would only take $10$ scans.
	\item But is there a way to balance this idea of best- and worst-case scenarios?
		That's what we'll cover in the last video, the bonus mentioned in the lab on how to optimize this algorithm to get one which requires fewer scans.
		See you then.
\end{itemize}

\section*{Exercise 2, Part 3}

\begin{itemize}
	\item Welcome to the final video on getting a faster algorithm that requires fewer scans.
	\item In the lab, we're teased with this more efficient process that involves only $14$ scans at most.
		So let's try and figure out how to find it.
	\item Remember that when we were determining the uncertainty in each scenario, the worst case was when we got a positive scan: this didn't narrow down our interval as much as when we had a negative scan.
	\item As another example, we can think back to one of the pre-lab questions: if we initially scan the interval $[0, 200]$ (with the fully functional zoom-lens), the best case would be a positive result.
		This means we narrow our uncertainty down to only $200$ feet.
		But a negative result means that we only narrow the tribble's position to the other $600$ feet.
	\item The solution in this case was to choose a scan which narrowed down the uncertainty to the same amount, no matter whether we received a positive or negative scan.
	\item Now, with the faulty zoom lens, is there some way we can similarly ensure that both the negative and positive scans narrow down our new search interval the same amount?
		This would reduce the worst-case uncertainty to the smallest that it can be!
	\item We unfortunately won't end up with as good of a reduction as half the interval, but it will be better than only $3/4$ of the length of the interval.
	\item Okay, so our task is to figure out how long our scanning interval should be so that the next interval we scan will be the same length, no matter whether we get a positive result or a negative result.
	\item So let's say we've narrowed down our search to the interval $[a, a + L]$ like before.
	\item We'll scan some range of this of length, let's say, $R$.
		So our scan will be of $[a, a + R]$.
	\item So, if we get a negative result, we know the tribble has to be in the part of the interval we didn't scan, $[a + R, a + L]$.
	\item If we get a positive result, it's either in the original interval $[a, a + R]$ or the ``error interval'' which is length $R / 2$ long.
		So it's either in $[a, a + R]$ or $[a + R, a + R + R/2]$, which comes out to $[a, a + R + R / 2]$.
	\item We said we wanted the lengths of these intervals to be the same, right?
		So the length of the narrowed down interval in the negative case is length $a + L -(a + R) = L - R$.
		The length of the narrowed down interval in the positive case is $a + R + R/2 - a = R + R/2$.
	\item Setting these equal and solving for $R$ gives $R = 3L/5$.
	\item So each scan, no matter positive or negative, reduces our uncertainty to $3/5$ of the original interval size!
		This is much better than $3/4$!
	\item If we do our analysis of the number of steps again, we now require
		\[
			800 \cdot \left( \frac{3}{5} \right)^n < 1,
		\]
		which, when we solve for $n$ like before, we find out
		\[
			13.09 < n.
		\]
		So with just $14$ scans instead of $24$, we're guaranteed to find the tribble, even with the faulty zoom lens.
	\item So that's it for this set of post-lab videos, thanks for watching and see you next time!
\end{itemize}

\end{document}
