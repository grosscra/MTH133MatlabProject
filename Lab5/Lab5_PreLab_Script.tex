\documentclass[12pt]{article}
\usepackage{fancyhdr}
\usepackage{geometry}
\usepackage{graphicx}
\usepackage{enumitem}
\usepackage{amsmath}

\geometry{letterpaper, scale=0.80, hmarginratio={1:1}, vmarginratio={1:1}, headheight=20pt, headsep=0.2in, footskip=0.5in}

\pagestyle{fancy}
\fancyhf{}
\rhead{July 15, 2019}
\lhead{MTH133}
\chead{\large \bf Lab 5 Pre-Lab Script}
\cfoot{\thepage}

\setlength\parindent{0pt}
\setlength\parskip{15pt}

\begin{document}

\section*{Introduction}

\begin{itemize}
	\item Discuss learning goals
		\begin{itemize}
			\item Welcome to the Lab 5 Pre-Lab where we will study divergent sequences originating from economic modeling.
			\item In particular, we will see how real-life phenomenon can be represented as a sequence.
			\item We will also see how tweaking our model can result in various types of behavior in those sequences, including cycles and chaos.
		\end{itemize}
	\item Background
		\begin{itemize}
			\item Before we start with that though, let's discuss some simpler, more familiar examples.
			\item \emph{(Write down formulas and plot sequences as they're discussed.) }
			\item Remember from class a simple convergent sequence where each term is given by $a_n = 1/n$.
				The first few terms look like
				\[
					1, \frac{1}{2}, \frac{1}{3}, \frac{1}{4}, \ldots
				\]
				This sequence converges to $0$ as $n \rightarrow \infty$.
			\item On the other hand, let's come up with a divergent sequence.
				This first thing that springs into my head is a sequence where the terms get bigger and bigger.
				How about something like $a_n = 2^n$, where the first few terms are
				\[
					2, 4, 8, 16, 32, \ldots
				\]
			\item However, this is not the only type of divergent sequence!
				Remember, a sequence is divergent so long as it just doesn't converge.
				So something that doesn't necessarily fly off to infinity can still be divergent.
				Remember the example of $a_n = (-1)^n$
				\[
					-1, 1, -1, 1, \ldots
				\]
				The terms in the sequence just oscillate back and forth, never getting closer and closer to any number.
				So this is a divergent sequence as well.
		\end{itemize}
	\item Setup and notation
		\begin{itemize}
			\item But in this lab, we won't be studying sequences that come from a formula as simple as these.
			\item We're going to construct a model of an economic phenomenon which we will see results in a sequence that we can qualitatively analyze as convergent or divergent.
			\item In particular, our setting is a free market where we are trying to model the number of producers of a good in some industry, for example the number of car manufacturers.
			\item \emph{(Write out the definition of $X(t)$, and label example sequence.)}
			\item We'll say the number of producers is written by $X(t)$ where $t$ is the number of financial quarters since a fixed date (for example, the first quarter 1980).
				So for our purposes, $t$ should always be a whole number, and for example, $X(0)$ is the number of producers in the first quarter of 1980, $X(1)$ is the number of producers in the second quarter of 1980, $X(4)$ is the number of producers in the first quarter of 1981, and so on.
			\item We see that if we write down the number of producers in the first quarter, then the second, and so on, we get a numerical sequence.
				The number of quarters since our starting point tells us which number in the sequence we're talking about.
		\end{itemize}
	\item Defining the model
		\begin{itemize}
			\item Now how do we actually calculate the values for $X(t)$?
			\item \emph{(Add a break in the example sequence to stretch out to 100 years down the line and continue the sequence.)}
			\item It would be impossible to come up with an exact formula that captured every possible intricacy of an industry so well that we could just plug in a time, and get the number of producers.
				If something like this were possible, we could predict exactly how many companies will be manufacturing smartphones in 100 years!
				We could predict the rise and fall of any industry and likely make some good money in the stock market.
			\item Since something like this is impractical, we'll start out small, and make some assumptions that capture simple behavior.
			\item The main idea is something like supply and demand.
			\item \emph{(Draw sequence plot with market equilibrium as dotted line.
				Show point above, moving down, then point below, moving above.)}
			\begin{itemize}
				\item There will be a market equilibrium: a number of producers that is demanded.
				\item If there are more producers than this market equilibrium, the number of producers will go down in the next quarter.
				\item On the other hand, if there are fewer producers than the market equilibrium, the number of producers will go up in the next quarter.
			\end{itemize}
			\item Just how much the number of producers swings up or down depends on the market and something called \emph{volatility}, and is something you'll investigate further in the lab.
			\item To mathematically capture this type of behavior, we'll use something called a response function.
			\item \emph{(Graph sample response function, label, and point out behavior.)}
			\item The response function will have a big long formula defined in the lab, but only two real assumptions are necessary:
				\begin{itemize}
					\item The function must be decreasing.
					\item The function must always be positive.
				\end{itemize}
			\item As we discuss how it's used, keep these points in mind, and try and figure out why these have to be the case.
			\item \emph{(Label $x$ axis.)}
			\item Now, what goes into the response function is the number of producers at a given time.
			\item What comes out is the growth rate of producers moving into the next quarter.
			\item \emph{(Describe each term as the full equation is written out.)}
			\item Mathematically, that says that the number of producers in the next quarter
				\[
					X(t + 1)
				\]
				is given by the number of producers in the current quarter
				\[
					X(t)
				\]
				times the value of the response function at that number of producers
				\[
					X(t + 1) = X(t) \cdot R(X(t)).
				\]
		\end{itemize}
\end{itemize}

\section*{Example}

\begin{itemize}
	\item To get a feel for this, let's try an example.
	\item Like I said before, the actual formula for the response function is long and tough to deal with, so we'll just treat it like a ``black box'' in MATLAB.
		We'll give MATLAB a number, it'll spit out the value of the response function, and we won't worry about how it calculated that number for now.
	\item For those interested in quantitative finance or investment banking, those jobs basically boil down to using sophisticated mathematics to guess what the response function, except they also have to include government policy and hundreds of other related industries as contributing factors.
	\item \emph{(Start a running tally of the sequence up at the top.
		Have sketch of response function below.
		For each calculation, draw on response function as in lab document.
		Erase for each successive value in the sequence.)}
	\item So, to start, let's say the number of producers in the industry is 5.
		Mathematically, we say $X(0) = 5$, this is the first term of our sequence.
	\item Now, we plug 5 into the response function, and based off of the formula in MATLAB, the value we get back is $3$.
	\item So we calculate the next value of $X$ by multiplying these together:
		\[
			X(1) = X(0) \cdot R(X(0)) = 5 \cdot 3 = 15.
		\]
		So the number of producers in the second quarter is 15.
	\item Let's try this another time.
		If we plug our new value 15 into the response function, MATLAB gives us $0.8$.
		So we calculate the next value as
		\[
			X(2) = X(1) \cdot R(X(1)) = 15 \cdot 0.8 = 12.
		\]
		So the number of producers is 12.
	\item \emph{(Plot the sequence like the simple ones earlier.)}
	\item So we start at 5 and jump way up to 15, so we were originally below the market equilibrium.
	\item Then from 15 we went down to 12, which means we're above the market equilibrium.
	\item We can keep going (and actually have MATLAB do this all for us), to see what the number of producers is going to end up looking like, convergent, divergent, cyclical, whatever!
		And like we said earlier, we'll make some changes to the response function to handle different types of volatility, to see how that affects all of this behavior.
\end{itemize}

\end{document}
