\documentclass[12pt]{article}
\usepackage{fancyhdr}
\usepackage{geometry}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{enumitem}

\geometry{letterpaper, scale=0.80, hmarginratio={1:1}, vmarginratio={1:1}, headheight=20pt, headsep=0.2in, footskip=0.5in}

\pagestyle{fancy}
\fancyhf{}
\rhead{Feb. 26, 2019}
\lhead{MTH133}
\chead{\large \bf Lab 5: Pre-Lab}
\cfoot{\thepage}

\setlength\parindent{0pt}
\setlength\parskip{15pt}

\definecolor{grayBG}{gray}{0.85}
\newcommand\answerbox[1]{\raisebox{-0.8ex}{\textcolor{grayBG}{\rule{#1}{3ex}}}}

\begin{document}

\section*{Learning Goals}

In class, typical examples of divergent sequences and series are those whose values fly off to infinity. 
In real life, however, divergence is more frequently associated with meandering behavior that doesn't fly off to infinity but also doesn't settle down toward one number.
The simplest example of this is the cyclical behavior of the sequence
\[1, -1, 1, -1, 1, -1, ...\]
which remains always bounded but nevertheless does not converge. 

Real-life examples of bounded divergent sequences don't usually exhibit the same level of regularity, and this is what makes it hard to predict the future.

Today you will learn about \textbf{convergence and divergence}, especially the \textbf{cyclical booms and busts} in a model of economics.
You will see examples of the \textbf{butterfly effect} that comes up in \textbf{chaos theory} and see why it is difficult to profit from short-term investments in the stock market. 

\section*{A model of the market}

We describe here a mathematical model that captures some essential features of a \textbf{capitalist free market}.
The main unknown is $X(t)$, the \textbf{number of producers in a given industry}. 
Here $t$ counts the \textbf{number of financial quarters} since a fixed date (say first quarter of 1980), so is always a whole number. 

Since financial reports are released \textbf{quarterly} in the United States, based on the reported earnings of the previous quarter some companies may decide to enter the market, and some may decide to leave. 

Our model aims to describe the following process:

\begin{itemize}
\item When the number of producers is below market equilibrium, additional numbers will enter the market.
\item When the number of producers is above market equilibrium, some fraction will exit the market. 
\end{itemize}

Fundamental to the model is the \textbf{response curve} $R(X)$ that relates the number of industry players in the next quarter to the number active in the current quarter.
In other words, if at the current quarter the number of industry players is $X(t)$, then the number of industry players in the next quarter can be computed from

\[X(t+1) = R(X(t)) \cdot X(t).\]

For our purposes, the formula of the response function $R(X)$ is not too important as we will have MATLAB do most of the calculations for us, but we do require that the function exhibits the following features:

\begin{itemize}
\item $R(X)$ is a decreasing function of $X$.
\item $R(X)$ is never less than zero. 
\end{itemize}

\section*{The first few terms of the sequence}

To show how the response function dictates the growth of the number or producers in the industry, we will calculate the values of $X(t)$ in the first three quarters.

We will use a sample response function $R(X)$ given by the plot below.
Remember that it should be decreasing and never less than zero.

\begin{figure}[ht!]
	\centering
	\includegraphics[scale = 0.4]{Figures/1.pdf}
\end{figure}

We'll suppose that during the first quarter, there are 5 producers in the industry, that is, $X(0) = 5$. 
Reading from the graph below, we get that $R(5)$ is 3.

\begin{figure}[ht!]
	\centering
	\includegraphics[scale = 0.38]{Figures/2.pdf}
\end{figure}

Using our equation \(X(t+1) = R(X(t)) \cdot X(t)\), we calculate that the number of producers in the industry during the second quarter is
\[
	X(1) = R(X(0)) \cdot X(0) = 3 \cdot 5 = 15.
\]

Going back to the response curve below, plugging in the new value of $X$ gives $R(15) = 0.8$.

\begin{figure}[ht!]
	\centering
	\includegraphics[scale = 0.38]{Figures/3.pdf}
\end{figure}

Using the same equation again, we have the number of producers in the third quarter is
\[
	X(2) = R(X(1)) \cdot X(1) = 0.8 \cdot 15 = 12.
\]
We now have the first three terms of a sequence which we can analyze like any other.
During the lab itself, we will look at the same model but with \textbf{different degrees of volatility} which will affect how the number of producers changes based on their distance from the market equilibrium.
\end{document}
