\documentclass[12pt]{article}
\usepackage{fancyhdr}
\usepackage{geometry}
\usepackage{graphicx}
\usepackage{enumitem}
\usepackage{listings}
\usepackage{color}
\definecolor{mygreen}{rgb}{0,0.6,0}
\lstset{basicstyle=\ttfamily,language=Matlab,keywordstyle=\color{blue},commentstyle=\color{mygreen}}
\usepackage{alltt}

\geometry{letterpaper, scale=0.80, hmarginratio={1:1}, vmarginratio={1:1}, headheight=20pt, headsep=0.2in, footskip=0.5in}

\pagestyle{fancy}
\fancyhf{}
\rhead{Jan. 15, 2019}
\lhead{MTH133}
\chead{\large \bf Lab 1: Pre-Lab}
\cfoot{\thepage}

\setlength\parindent{0pt}
\setlength\parskip{15pt}

\begin{document}

\section*{Learning Goals}

As discussed in class last week, integration really is a process of divide-and-conquer: you wish to compute a complicated quantity, and to do so, you first chop it up into many small pieces, each of which easier to compute, then you add everything back together.

\emph{Exact integration} (or, finding the antiderivative) requires thinking about the limiting case where the division step divides into \emph{finitely many} pieces each of which are in fact \emph{infinitesimally small}.
In most practices, if you just want a ``good enough approximation'' (which would almost always be the case in science and engineering applications), you can get away with just \emph{very many} pieces each of which are \emph{very small}.
This is what we will explore in this lab.

\section*{Integrals versus Sums}

First let us revisit the idea that the definite integral of a function is defined via a limiting procedure.
Given a function $f(x)$, we can draw below it a number of rectangles and compute the sums of the areas of the rectangles.

This is a tedious calculation to do by hand, but we will be using \texttt{MATLAB} to do the work for us and help visualize our setup.
This Pre-Lab will walk you step by step through the code we'll use, and compare it to the calculations we're familiar with.

Let's start with an example:

Take our function to be $f(x)=\sin(x^2)+1$, and our domain to be the interval $[0,3]$.
Using the following code, we are able to plot the graph of the function.

\begin{center}
\begin{tabular}{c}
\begin{lstlisting}
f = @(x) sin(x.^2) + 1 % Define our function
a = 0 % Left endpoint
b = 3 % Right endpoint
fplot(f, [a, b]) % Plot the functions
hold on % Get ready to plot more
\end{lstlisting}
\end{tabular}
\end{center}

We will now draw $N$ rectangles below the graph to show the areas we add together to approximate the integral.
Each of these rectangles will touch the graph with their \textbf{top left} corners.

In this case, we will set $N = 6$, but with the magic of \texttt{MATLAB}, in the actual lab document, we are able to change $N$ on the fly with a slider.
The code to plot these rectangles is given below with explanations in the comments.

\begin{center}
\begin{tabular}{c}
\begin{lstlisting}
N = 6 % Number of divisions

% If we divide the interval [a, b] into N pieces,
% each piece would have width
width = (b - a) / N

% The command below says to look at all values of x,
% starting from a, increasing by the variable 'width'
% each time, until the final value where x = b - width
for x = a:width:(b - width)
    % For each of the x values, we draw a rectangle 
    % with the lower left corner at coordinate (x, 0),
    % width the variable 'width', and height the value f(x).
    rectangle(`Position', [x 0 width f(x)], `EdgeColor', `r')
end
hold off % Done plotting 
\end{lstlisting}
\end{tabular}
\end{center}

\section*{Computing area under curve}

As seen from the figure below, with the aid of these rectangles, we can compute an approximation to the area under the curve for the function $f(x)$.

\begin{figure}[ht!]
	\centering
\includegraphics[scale = 0.42]{Figures/lab1-graph.pdf}
\end{figure}

%Incidentally, the value of the integral $\int_{0}^{3} {\sin(x^2)+1} \;dx \approx 3.773562526893769...$

To compute the corresponding Riemann sum, we can use the code

\begin{center}
\begin{tabular}{c}
\begin{lstlisting}
approximated_integral = sum(f(a:width:(b-width)) .* width)
\end{lstlisting}
\end{tabular}
\end{center}

Let's explain the pieces in this command a little, working from the inside out: 
\begin{itemize}
	\item Remember from the rectangle plotting above, the command \texttt{a:width:(b - width)} is the list of $x$ values starting from \texttt{a}, ending at \texttt{b - width}, spaced \texttt{width} apart.
	\item The command \texttt{f(a:width:(b-width))} plugs this list of $x$ values into the function $f(x)$, producing the corresponding list of $f(x)$ values.
	\item We then multiply these $f(x)$ values by \texttt{width} and add them all up.
\end{itemize}

So for our example with $N = 6$, the this command computes 
\[
	f(0)\cdot0.5+f(0.5)\cdot0.5+f(1)\cdot0.5+f(1.5)\cdot0.5+f(2)\cdot0.5+f(2.5)\cdot0.5
\]
which we can also write, using Sigma notation, as
\[
	\sum_{j=0}^{5}f(0+j\cdot\Delta{x})\cdot\Delta{x}
\]
where the width $\Delta{x}=(3-0)/6=0.5$.
Notice that this looks almost exactly the same as our \texttt{MATLAB} code calculating our approximate integral!

More generally, for arbitrary values of $N$, the number of divisions, the area can be written as
\[
	\sum_{j=0}^{N-1}f(0+j\cdot(3/N))\cdot(3/N)
\]
And as a reminder, the integral as described in Calculus I is the limiting value as the number of divisions go to infinity
\[
	\int_{a}^{b} f(x) dx = \lim_{N\to\infty}\left[\sum_{j=0}^{N-1}f\left(a+j\cdot\frac{b-a}{N}\right)\cdot\frac{b-a}{N}\right].
\]

In the main lab, we'll continue to play with these ideas, linking them with \texttt{MATLAB} to do the heavy computational lifting for us.
\end{document}
