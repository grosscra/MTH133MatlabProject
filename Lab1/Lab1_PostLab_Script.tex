\documentclass[12pt]{article}
\usepackage{fancyhdr}
\usepackage{geometry}
\usepackage{graphicx}
\usepackage{enumitem}
\usepackage{amsmath}

\geometry{letterpaper, scale=0.80, hmarginratio={1:1}, vmarginratio={1:1}, headheight=20pt, headsep=0.2in, footskip=0.5in}

\pagestyle{fancy}
\fancyhf{}
\rhead{June 13, 2019}
\lhead{MTH133}
\chead{\large \bf Lab 1 Post-Lab Script}
\cfoot{\thepage}

\setlength\parindent{0pt}
\setlength\parskip{15pt}

\begin{document}

\section*{Exercise 1}

\begin{itemize}
	\item Welcome to the videos for Lab 1.
	\item Today we'll be discussing numerical integration which is the process of approximately figuring out the area under the curve of a graph.
	\item We're going to start by getting a feel for Riemann sums and how they interact with some functions we'd like to find the area under.
	\item \emph{(Highlight the parts of the question that correspond to what is being said.)}
	\item We're first asked to consider the definite integral of an increasing function over the interval from $0$ to $10$.
		The goal is to determine whether the Riemann sum here, which is built up from rectangles touching the function with their left endpoints, will overestimate or underestimate the integral.
	\item Well let's start with a little sketch.
	\item \emph{(Sketch out an increasing functions with rectangles underneath.)}
	\item I don't want to draw all 100 rectangles, so I'll just do a few and see if it gives us the right idea.
	\item An increasing function is going to look like this, getting bigger as the $x$-values get bigger.
	\item Now if we draw rectangles underneath it so that the left endpoints touch the function, we see that the function will always overshoot the rest of each rectangle.
	\item So when we fill in the area given by the rectangles, we'll be missing all the area above the rectangle and below the function.
		Thus, we get an underestimate of the area.
	\item \emph{(Same for decreasing function, can skim through.)}
	\item Now, let's try a decreasing function.
	\item The same idea carries over, but in reverse.
	\item As we draw the rectangle, it will always stay higher than the function, because the function will decrease over this little interval.
	So this will give us an underestimate.
\end{itemize}

\section*{Exercise 2}

\begin{itemize}
	\item Now we get to using MATLAB to help us both visualize and calculate the mathematics we're talking about which will be a major theme in all of the labs this semester.
	\item \emph{(Have MATLAB open to look at the code and the comments.)}
	\item So we're given this piece of code which plots and calculates the left-Riemann sums.
		We'd like to modify it to do the same thing, but with Riemann sums where the right endpoint touches the function, also called right-Riemann sums.
	\item So let's look at the important piece of code which plots the rectangle:
		\[
			\texttt{rectangle(`Position', [x 0 width f(x)], `EdgeColor', `r')}
		\]
	\item We only care about the position of each rectangle which is specified by the list of numbers \texttt{[x 0 width f(x)]}.
	\item \emph{(Draw out how these numbers relate to the rectangle drawing.)}
	\begin{itemize}
		\item We see in the comments that the first two numbers say what the bottom left point of the rectangle is.
		\item Then the width of the rectangle comes next.
		\item Then the height.
	\end{itemize}
	\item Our rectangles should look pretty much the same, just have different heights.
		So the only thing we need to change is the \texttt{f(x)} part.
	\item \emph{(Draw out the partition on a little number line.)}
	\item So remember what \texttt{x} represents in our expression: we take every $x$-value starting with \texttt{a} and going up to \texttt{b - width} separated by \texttt{width} amount.
		And for each of these \texttt{x} values we draw a rectangle specified by the numbers \texttt{[x 0 width f(x)]}.
	\item So if we start with $x$ as the left-most side of the rectangle, the right-most side will be at $x + width$.
	\item The height of the function here should be the height of the rectangle.
	\item So all we need to do is replace \texttt{f(x)} by \texttt{f(x + width)}.
	\item Now, to get the sum right, we have to make sure that we add up all the function values at the right end points.
	\item \emph{(Use partition sketch to show the starting and ending points.)}
	\item Notice that this will now start with \texttt{a + width} and go up to \texttt{b}.
		They'll still be separated by \texttt{width} amount though.
	\item \emph{(Go back to MATLAB.)}
	\item So where we were originally starting with \texttt{a}, we'll change that to \texttt{a + width}.
		And where we were originally ending with \texttt{b - width}, we'll change that to \texttt{b}.
		But we keep the amount we skip by the same.
	\item And that's it!
		If we run this code, we see that our new rectangles line up and we calculate the sum of there areas right underneath.
\end{itemize}

\section*{Exercise 3}

\begin{itemize}
	\item Now let's take this coding knowledge and translate it into mathematical notation.
	\item If we want to write down the formula for a right-Riemann sum, we should use the same tactics:
		\begin{itemize}
			\item We'll be adding up a bunch of function values multiplied by the width of the rectangles $(b - a) / N$, where
			\item The $x$-values start at $a + (b - a) / N$
			\item Go up to $b$,
			\item And are $(b - a) / N$ apart.
		\end{itemize}
	\item And this should be $N$ things we're adding up, the area of each rectangle.
	\item If I just wrote this out without Sigma notation, this would look something like
		\[
			f\left(a + \frac{b - a}{N} \right) \cdot \frac{b - a}{N} + f\left(a + 2 \frac{b - a}{N} \right) \cdot \frac{b - a}{N} + \ldots + f(b) \cdot \frac{b - a}{N}.
		\]
	\item We see that each step is made by bumping up the number of $(b - a) / N$'s we're adding to $a$ inside $f$ each time.
	\item So our sigma notation can be something like
		\[
			\sum f\left(a + j \cdot \frac{b - a}{N} \right) \cdot \frac{b - a}{N}.
		\]
	\item All that's left is figuring out where to start and end this sum.
	\item We know that there is one $(b - a) / N$ to start with, so $j$ should be $1$.
	\item We also know that there are $N$ rectangles, so we should count up to $N$ to get them all.
		We can also check that plugging in $N$ for $j$ in the $f$ part gives the function value in the last term of the sum, f(b): 
		\[
			f\left(a + N \cdot \frac{b - a}{N} \right) = f(a + b - a) = f(b).
		\]
	\item So our final answer is
		\[
			\sum^{N}_{j = 1} f\left(a + j \cdot \frac{b - a}{N} \right) \cdot \frac{b - a}{N}.
		\]
	\item Notice that this formula is almost exactly the same as the one for the left-Riemann sums, except that it starts one later and ends one later.
	\item This is exactly the same process we used to write the code to calculate this sum.
	\item We changed \texttt{a} to \texttt{a + width} (which is one tick mark down the line) and changed \texttt{b - width} to \texttt{b} (which is also one tick mark down the line).
\end{itemize}

\section*{Exercise 4}
\begin{itemize}
	\item Now we're going to try and relate the left and right sums together.
	\item This expression $B(N) - A(N)$ just means take the right sum and subtract the left sum.
	\item \emph{(Pull up a graph of a right and left sum and annotate as we go.)}
	\item Let's see what this subtraction looks like graphically.
	\item If we look carefully, we see that the left sum has almost all of the rectangles that the right sum does, just staggered over to the right by one.
	\item So if we were subtracting these areas, we can cancel them out one-by-one.
	\item All that's left from the right-sum is the right-most rectangle which doesn't line up with anything, and all that's left from the left-sum is the left-most rectangle which doesn't line up with anything.
	\item The area of this right-most one is $f(b) \cdot \frac{b - a}{N} $ and the area of the left-most one is $f(a) \cdot \frac{b - a}{N} $.
	\item So when we subtract them, we get the formula we were after
		\[
			(f(b) - f(a)) \cdot \frac{b - a}{N}.
		\]
	\item Now let's try to go about it a different way, with the sigma notation sums.
	\item Writing this out, we see start with
		\[
			\sum^{N}_{j = 1} f\left(a + j \cdot \frac{b - a}{N} \right) \frac{b - a}{N}  - \sum^{N - 1}_{j = 0} f\left(a + j \cdot \frac{b - a}{N} \right) \frac{b - a}{N}.
		\]
	\item These sums are almost the same, they just start and end at different places.
	\item One important thing to realize though is that these are just sums with fancy notation.
	\item We can actually rewrite them a little bit, and have them line up a bit better.
	\item We see that every $j$ value covered in the right-sum is covered in the left sum, \emph{except} when $j = N$.
		So let's pull that out:
		\[
			\sum^{N - 1}_{j = 1} f\left(a + j \cdot \frac{b - a}{N} \right) \frac{b - a}{N} + f\left(a + N \cdot \frac{b - a}{N} \right) \frac{b - a}{N}.
		\]
	\item And every $j$ covered in the left-sum is covered in the right sum, \emph{except} $j = 0$.
		So same thing:
		\[
			f\left(a + 0 \cdot \frac{b - a}{N} \right) \frac{b - a}{N} + \sum^{N - 1}_{j = 1} f\left(a + j \cdot \frac{b - a}{N} \right) \frac{b - a}{N}.
		\]
	\item Now, when we rewrite the sigma notation and distribute the negative, we get
		\begin{align*}
			\sum^{N - 1}_{j = 1} f\left(a + j \cdot \frac{b - a}{N} \right) \frac{b - a}{N} &+ f\left(a + N \cdot \frac{b - a}{N} \right) \frac{b - a}{N} - f\left(a + 0 \cdot \frac{b - a}{N} \right) \frac{b - a}{N}  \\&- \sum^{N - 1}_{j = 1} f\left(a + j \cdot \frac{b - a}{N} \right) \frac{b - a}{N}.
		\end{align*}
	\item Now the two sigma pieces cancel out, and we're just left with
		\[
			f(b) \cdot \frac{b - a}{N} - f(a) \cdot \frac{b - a}{N},
		\]
		just like we wanted.
	\item Notice here that we actually did the same thing as in the graphical explanation.
	\item We separated off the first and last rectangles, and canceled out all the rectangles in the middle.
		This left us with the same amount of area left over.
	\item And if we're interpreting what this means, this says that the difference between the right and left sums will always get smaller as we increase $N$, because everything on the right-hand-side stays the same except it will be divided by bigger and bigger $N$ values.
	\item In the case of maybe an increasing or decreasing function where the actual values of the integrals lies in between the values of the right and left sum, we know that these right and left sums will eventually squeeze together getting very close to the value of the actual integral as $N$ increases.
	\item So if we want a good enough approximation of the integral without doing the fundamental theorem of calculus (which may be impossible if the function doesn't have a simple enough anti-derivative), we can just use a big enough $N$, and plug our function into either of these formulas.
\end{itemize}

\end{document}
