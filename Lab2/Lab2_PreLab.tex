\documentclass[12pt]{article}
\usepackage{fancyhdr}
\usepackage{geometry}
\usepackage{graphicx}
\usepackage{enumitem}
\usepackage{hyperref}
\usepackage{amsmath} 

\geometry{letterpaper, scale=0.80, hmarginratio={1:1}, vmarginratio={1:1}, headheight=20pt, headsep=0.2in, footskip=0.5in}

\pagestyle{fancy}
\fancyhf{}
\rhead{Jan. 15, 2019}
\lhead{MTH133}
\chead{\large \bf Lab 2: Pre-Lab}
\cfoot{\thepage}

\setlength\parindent{0pt}
\setlength\parskip{15pt}

\begin{document}

\section*{Learning Goals}

In this lab, you will learn about how calculus can be applied to solve problems in \textbf{rocketry}.

As you will see, the kinds of functions that come up in rocket science change in discrete steps.
It makes sense to talk about the value of the function at the first step, the second step, etc.
But not at the first-and-a-half-step.
This is different from the setting in your calculus class where functions can take arbitrary numbers as arguments.
However, we will see that by linking these discrete steps to Riemann sums, we can \textbf{use the integration techniques learned in class to help analyze these situations.
In particular, we will make use of the integral of $1/x$ as seen in section 6.2.}

You will again see this idea, where functions that change in discrete steps are approximated by, or are used to approximate, continuous functions, later in this course, when we get to Chapter 11 on sequences and series.

\section*{Baseball Rocketry}

Rockets work by \textbf{Newton's Third law}: ``for every action there is an equal but opposite reaction.''
The rocket engine burns rocket fuel, and hurls the exhaust particles backwards (\emph{the action}).
The recoil from ejecting the exhaust pushes the rocket forward (\emph{the reaction}).

To understand the mathematics behind this better, we shall enlist the help of Robin Roberts, legendary Phillies pitcher and the only Spartan inducted to the Baseball Hall of Fame.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.2\linewidth]{Figures/RobinRoberts1953bowman.jpg}
	\caption{\href{https://commons.wikimedia.org/wiki/File:RobinRoberts1953bowman.jpg}{Image public domain.}}
	\label{fig:RobinRoberts}
\end{figure}

Robin Roberts will be our rocket engine; we will put him on skates (see Figure~\ref{fig:Figures/StoppedClipart}) and give him a giant bag (not pictured) of baseballs for fuel.

\begin{figure}[ht]
	\centering
	\includegraphics[height=0.25\textheight]{Figures/StoppedClipart.jpg}
	\caption{Roberts stopped on ice skates.}
	\label{fig:Figures/StoppedClipart}
\end{figure}

Roberts is famous for his consistent fast ball.
After his pitch, the ball whirls off to the left, while Roberts recoils ever-so-slightly to the right (see Figure~\ref{fig:Figures/ThrownClipart}).

\begin{figure}[ht]
	\centering
	\includegraphics[height=0.20\textheight]{Figures/ThrownClipart.jpg}
	\caption{Roberts after throwing a baseball.}
	\label{fig:Figures/ThrownClipart}
\end{figure}

To find out the speed of the recoil, we will need a few pieces of information:
\begin{itemize}
	\item The number $N$ of baseball he has before the pitch.
	\item The starting velocity of our Robin-Roberts-Rocket before the pitch $v_\mathrm{before}$
	\item The mass of a baseball $m_\mathrm{b}$.
		Regulation baseballs are about 5 ounces, or 145 grams.
	\item The speed of Roberts' fastball $v_\mathrm{b}$.
		This is around 90 mph or 40 meters/sec.
	\item The body mass of Roberts $M_\mathrm{R}$.
		During his pro baseball career, he is listed at 190 lbs, or 86 kg.
\end{itemize}

If we use $\Delta v$ to represent the change in velocity after \emph{one particle} of exhaust (or baseball) is thrown, the laws of physics tell us that 
\begin{equation}
	\label{eq:DeltaV}
	\Delta v = v_\mathrm{after} - v_\mathrm{before} = \frac{m_\mathrm{b} \cdot v_\mathrm{b}}{M_\mathrm{R} + (N - 1) \cdot m_\mathrm{b}}.
\end{equation}
(For those interested, the derivation of this equation can be found in the appendix.)

One important note is that \textbf{the denominator is the mass of everything left on the rocket after the pitch}, that is, the mass of Roberts and the rest of the baseballs.
This means that the change depends on how much fuel is currently on board.
\begin{itemize}
	\item For actual rockets, the number $M_\mathrm{R}$ represents the mass of all non-expendable parts of the space-craft.
		This includes the fuel tank, the engine itself, as well as any payload.
	\item The remaining fuel onboard is given by $(N - 1) \cdot m_\mathrm{b}$.
		The number $(N - 1)$ represents the number of units of fuel on board, and $m_\mathrm{b}$ is the mass per unit of fuel.
\end{itemize}

To test whether this equation matches your intuition, imagine Roberts throwing some large number of baseballs.
Think about which baseball contributes to his final speed more, the first or last.
Does this match with what the equation tells us?

\section*{The fundamental question}
The fundamental question of rocket design is: \textbf{given a fixed amount of fuel, what is the maximum speed I can get to on the rocket}?

In terms of our baseball rocket, we want to know: if we start with $N$ baseballs and the rocket moving at speed $v_0$, what will be the speed after all $N$ baseballs are pitched.

What makes this question hard to answer is that the formula for $\Delta v$ depends on the \emph{current} amount of fuel, which changes throughout!

When we start with $N$ balls, what ends up happening is that the final velocity $v_f$ can be found by adding all of the changes in velocity due to each individual pitch to the initial velocity $v_0$:
\begin{align*}
	v_f &= v_0 + \frac{m_\mathrm{b} v_\mathrm{b}}{M_\mathrm{R} + (N - 1) \cdot m_\mathrm{b}} + \frac{m_\mathrm{b} v_\mathrm{b}}{M_\mathrm{R} + (N - 2) \cdot m_\mathrm{b}} + \ldots + \frac{m_\mathrm{b} v_\mathrm{b}}{M_\mathrm{R} + (N - N) \cdot \mathrm{b}}.
\end{align*}

We can also write this in Sigma notation as
\[
	v_f = v_0 + \sum^{N}_{n = 1} \frac{m_\mathrm{b} v_\mathrm{b}}{M_\mathrm{R} + (N - n) \cdot m_\mathrm{b}}.
\]

\section*{Integrals and sums in reverse}

As we will see in the lab, finding this final velocity by adding up a bunch of terms can become too difficult, even for a computer.
However, we can simplify this task by \emph{approximating} the sum with an integral, the exact opposite of what we did in the previous lab.

The key insight is to represent a sum like
\[
	K(1) + K(2) + K(3) + \ldots + K(N).
\]
as a Riemann sum of the function $K(x)$ where the rectangles have width $\Delta x = 1$ and height $K(x)$.
Comparing with Figure~\ref{fig:RiemannSum}, this sum is approximately the same as the integral of $K(x)$ which may be easier to compute.
\begin{figure}[ht]
	\centering
	\includegraphics[width=0.4\linewidth]{Figures/RiemannSum.jpg}
	\caption{A sum represented as a Riemann sum.}
	\label{fig:RiemannSum}
\end{figure}

\section*{Appendix}

The total momentum of the system before the pitch is given by the total mass times the total velocity.
The total mass before the pitch is
\[
	\text{Mass of Roberts} + \text{Mass of Baseballs} = M_\mathrm{R} + N \cdot M_\mathrm{b} = 86 + 0.145 \cdot N \text{ kg}.
\]
So the momentum is given by
\[
	\text{Total Initial Momentum} = \text{Total Mass} \times \text{Initial Velocity} = (86 + 0.145 \cdot N) \cdot v_\mathrm{before} \text{ kg m/s}.
\]
The speed of our rocket after the pitch can be computed by conservation of momentum.
The total momentum after the pitch is the sum of two parts, the momentum of the thrown baseball and the momentum of the Robin-Roberts-Rocket, including the bag of baseballs for rocket fuel.
\[
	\text{Momentum of baseball} = m_\mathrm{b} \cdot (v_\mathrm{before} - v_\mathrm{b}) = 0.145 \cdot (v_\mathrm{before} - 40) \text{ kg m/s}.
\]
Notice that the speed of the baseball is measured as $(v_\mathrm{before} - 40)$ meters/second.
This is because our rocket engine spews out exhaust at a speed relative to the engine itself; in other words, Roberts' fastball is traveling \emph{away from Roberts} at 40 meters/sec.
To convert that relative speed to an absolute speed, we need to factor in how fast the rocket is already moving.
\begin{align*}
	\text{Momentum of rocket with remaining fuel on board} 
		&= (M_\mathrm{R} + (N - 1) \cdot m_\mathrm{b}) \cdot v_\mathrm{after} \\
		&= (86 + 0.145 \cdot (N - 1)) \cdot v_\mathrm{after} \text{ kg m/s}.
\end{align*}
Here we used $N - 1$ for the number of balls onboard since one baseball has been pitched.
A little bit of algebra tells us that the change in speed is given by
\[
	\Delta v = v_\mathrm{after} - v_\mathrm{before} = \frac{m_\mathrm{b}}{M_\mathrm{R} + (N - 1) \cdot m_\mathrm{b}} \cdot v_\mathrm{b}.
\]

\section*{A detailed example}

We start with 3 balls and $v_0 = 0$.
\begin{itemize}
	\item After throwing the first ball, our formula above says that the change in velocity of the rocket is
		\[
			\Delta v = \frac{0.145}{86 + 2 \cdot 0.145} \cdot 40 = 0.0672 \text{ meters/second.}
		\]
		This means that the speed after tossing 1 baseball is
		\[
			v_1 = v_0 + \frac{0.145}{86 + (N - 1) \cdot 0.145} = 0.0672 \text{ meters/second.}
		\]
	\item After throwing the second ball, the change in velocity of the rocket is
		\[
			\Delta v = \frac{0.145}{86 + 1 \cdot 0.145} \cdot 40 = 0.0673 \text{ meters/second.}
		\]
		This means that the speed after tossing 2 baseballs is
		\[
			v_2 = v_1 + \frac{0.145}{86 + (N - 2) \cdot 0.145} = 0.1345 \text{ meters/second.}
		\]
	\item Finally, after throwing the third ball, the change in velocity of the rocket is
		\[
			\Delta v = \frac{0.145}{86 + 0 \cdot 0.145} \cdot 40 = 0.0674 \text{ meters/second.}
		\]
		This means that the speed after tossing 3 baseballs is
		\[
			v_3 = v_2 + \frac{0.145}{86 + (N - 3) \cdot 0.145} = 0.2020 \text{ meters/second.}
		\]
\end{itemize}

\end{document}
