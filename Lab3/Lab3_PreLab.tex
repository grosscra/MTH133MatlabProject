\documentclass[12pt]{article}
\usepackage{fancyhdr}
\usepackage{geometry}
\usepackage{graphicx}
\usepackage{enumitem}
\usepackage{subcaption}
\usepackage{afterpage}
\usepackage{amsmath}

\geometry{letterpaper, scale=0.80, hmarginratio={1:1}, vmarginratio={1:1}, headheight=20pt, headsep=0.2in, footskip=0.5in}

\pagestyle{fancy}
\fancyhf{}
\rhead{February 5, 2019}
\lhead{MTH133}
\chead{\large \bf Lab 3: Pre-Lab}
\cfoot{\thepage}

\setlength\parindent{0pt}
\setlength\parskip{15pt}

\begin{document}

\section*{Learning Goals}

In this lab, we continue our exploration of \textbf{mathematical modeling} with a discussion of the logistic equation, using as a vehicle an hypothetical zombie attack.
The logistic equation is used to study \emph{population dynamics} and \emph{chemical reaction rates}, and is the next simplest feedback loop you will see besides simple exponential growth.

You will learn about modeling feedback loops using \textbf{differential equations}, expanding on the ideas of \textbf{exponential growth} from Section 6.5, and about analyzing differential equations near their steady states.

\section*{Zombie invasion!}

The plot below in Figure~\ref{fig:Attack} is from a simulation of a zombie attack on the MSU campus.
The MSU community numbers around 57,000 total.
Each point in the grid represents one individual.
Dark red means infected, light red means healthy.
As you can see, the MSU community is unlikely to survive past a month under this attack.

\afterpage{%
	\begin{figure}[ht]
		\centering
		\begin{subfigure}[b]{0.35\textwidth}
		\begin{center}
			\includegraphics*[width=\textwidth]{Figures/Day4.png}
		\end{center}
		\end{subfigure}
		\qquad \qquad
		\begin{subfigure}[b]{0.35\textwidth}
		\begin{center}
			\includegraphics*[width=\textwidth]{Figures/Day9.png}
		\end{center}
		\end{subfigure}
		\\[3ex]
		\begin{subfigure}[b]{0.35\textwidth}
		\begin{center}
			\includegraphics*[width=\textwidth]{Figures/Day12.png}
		\end{center}
		\end{subfigure}
		\qquad \qquad
		\begin{subfigure}[b]{0.35\textwidth}
		\begin{center}
			\includegraphics*[width=\textwidth]{Figures/Day16.png}
		\end{center}
		\end{subfigure}
		\\[3ex]
		\begin{subfigure}[b]{0.35\textwidth}
		\begin{center}
			\includegraphics*[width=\textwidth]{Figures/Day20.png}
		\end{center}
		\end{subfigure}
		\qquad \qquad
		\begin{subfigure}[b]{0.35\textwidth}
		\begin{center}
			\includegraphics*[width=\textwidth]{Figures/Day23.png}
		\end{center}
		\end{subfigure}

		\caption{Zombie attack over 23 days}
		\label{fig:Attack}
	\end{figure}
	\clearpage
}

Here's what we know (assume) about zombies:
\begin{enumerate}
	\item Unlike what you saw in \emph{The Walking Dead}, zombies are virtually indistinguishable from healthy people.
		\emph{So running away is not an option}.
	\item Everyday, each zombie will randomly choose another person on campus to bite (zombies also cannot tell zombies apart from healthy people).
		If the zombie bites a healthy person, the healthy person becomes a zombie.
		If the zombie bites a zombie, nothing happens.
	\item Zombie-ism is incurable.
		Once a zombie, always a zombie.
\end{enumerate}

(For those of you interested in public health, throughout, you can replace zombie-ism by some transmittable disease (say, HIV) and biting by the appropriate means of transmission.)

\section*{Modeling with Differential Equations}

If we assume that each day, a zombie is able to find another human to bite and infect, this would mean that the number of zombies would double each day.
This would lead us to believe that the rate of increase of the zombie population would be proportional to the number of zombies that already exist.
If the number of zombies is given by $Z$ and $r$ is the \emph{growth rate}, we describe this mathematically as
\begin{equation}
	\frac{dZ}{dt} = r Z,
	\label{eq:exponential}
\end{equation}
which you should recall from class as the \emph{differential equation} describing exponential growth.

But this type of behavior is expected only when the number of zombies is much smaller than the number of healthy individuals, since it's very likely that the zombies, even randomly choosing, will bite a healthy person.
However, as more and more humans are infected, you will see in simulations that the growth of the zombies slows down.
Take a minute to think about why this makes sense based on the assumptions we've made.
Thus, the growth rate is \emph{not constant}, as it decreases as the number of zombies increases.

We can describe this behavior in a differential equation as follows:
\begin{itemize}
	\item Let $Z$ be the number of zombies, and let $P = 57600$ be the number of total Spartans overall.
	\item Instead of using just $r$ for the growth rate, we now let the growth rate depend on the ability of zombies to successfully find humans to bite.
		In particular, if we call the initial rate of exponential growth $r_0$, we can replace $r$ in \eqref{eq:exponential}, the exponential growth differential equation above, with
		\[
			r_0 \cdot \frac{P - Z}{P}.
		\]
	\item The rate of change of $Z$, as a function of time, with the time $t$ measured in days can then be captured by the differential equation
		\begin{equation}
			\frac{dZ}{dt} = r_0 \cdot \frac{P - Z}{P} Z.
			\label{eq:logistic}
		\end{equation}
\end{itemize}

Notice that this new growth rate acts the way we wanted to.
When there are very few zombies, $(P - Z) / P \approx 1$, so the growth rate looks like
\[
	r_0 \cdot \frac{P - Z}{P} \approx r_0.
\]
This means that when there are very few zombies, the zombie population experiences exponential growth with growth rate $r_0$, just as we wanted.

On the other hand, when the population is almost entirely zombies $P - Z \approx 0$, and the growth rate looks like
\[
	r_0 \cdot \frac{P - Z}{P} \approx 0.
\]
Thus, there is little to no growth when there is a large number of zombies.

\end{document}
