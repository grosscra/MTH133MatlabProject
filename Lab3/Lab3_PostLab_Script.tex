\documentclass[12pt]{article}
\usepackage{fancyhdr}
\usepackage{geometry}
\usepackage{graphicx}
\usepackage{enumitem}

\geometry{letterpaper, scale=0.80, hmarginratio={1:1}, vmarginratio={1:1}, headheight=20pt, headsep=0.2in, footskip=0.5in}

\pagestyle{fancy}
\fancyhf{}
\rhead{June 10, 2019}
\lhead{MTH133}
\chead{\large \bf Lab 3: Post-Lab Script}
\cfoot{\thepage}

\setlength\parindent{0pt}
\setlength\parskip{15pt}

\begin{document}

\section*{Exercise 1}

\begin{itemize}
	\item Intro:
	\begin{itemize}
		\item Welcome to the post-lab videos for Lab 3.
		\item Just as a refresher, we're talking about a zombie attack overtaking MSU.
		\item We start with one infected person, then each day any zombies around will randomly pick somebody to bite.
			If they bite a human, that human is infected.
			If they bite a zombie, nothing happens.
		\item So our goal is to use pretty much just these facts along with some differential equations to make predictions about what the epidemic will look like.
	\end{itemize}
	\item Now, for Exercise 1, we're looking at a graph of two things:
	\begin{itemize}
		\item One is the percentage growth rate of the Zombie population, that is, the percentage increase in Zombies from the previous day.
			So if we had 4 yesterday, and now we have 6, we added 2 more zombies which is 50\% of the number we had yesterday.
			So the percentage growth rate is 50\%.
		\item The other thing on the graph is the percentage of MSU who is infected.
	\end{itemize}
	\item Now we first want to know why does this growth rate drop over time?
	\begin{itemize}
		\item Right, we see that originally, each day, we get about 100\% infection rate, so the number of infected people doubles each day.
		\item But as time goes on, this reduces until, near the end, almost nobody is getting infected.
		\item But of course, this makes sense with our key assumptions that the zombies aren't so smart and will bite other zombies.
		\item The more zombies there are, the more likely they are to bite other zombies and infect nobody.
		\item And towards the end here, we can expect that the zombies will have a tough time randomly infecting any human because there are just so few humans left!
		\item So drop in the growth rate is to be expected. 
	\end{itemize}
	\item Now, the second part of Exercise 1 asks us to relate these two quantities together:
	\begin{itemize}
		\item Right, they look very similar, but are kind of working opposite of one another.
		\item Just as we said before, as the number of people at MSU who are infected increases, as this percentage of the population which is infected goes up, the infection growth rate will go down because there are fewer humans to infect.
		\item If I wanted to be more quantitative with this idea, I could observe that my red graph looks like my blue graph but just flipped over and shifted up.
		\item \emph{In MATLAB:} So if I flip my red graph by making it negative, and then shift it up 100\%, I get approximately the same as my blue graph.
		\item This tells me that 
			\[
				G = 100 - I.
			\]
		\item I can also rewrite this as
			\[
				G + I = 100,
			\]
			that is, the two quantities always seem to add up to make 100\%.
			If the growth rate is really small, then the infection percentage is really large to make up for it (and vice versa).
		\item And this is to be expected from what we see on the graph.
	\end{itemize}
\end{itemize}

\section*{Exercise 2}

\begin{itemize}
	\item Okay, now in Exercise 2, we're trying to find the \emph{initial} growth rate of the zombie increase, when it's still working like exponential growth with no tapering off or anything.
	\item The exercise has us modeling this as exponential growth, with this equation
		\[
			Y' = r_0 Y.
		\]
	\item And we know that this equation is like doubling exponential growth because originally we have one thing, $Y(0) = 1$, and after one day or unit of time, we have two things, $Y(1) = 2$.
	\item So all this comes down to is applying what we know about this differential equation and some algebra:
	\begin{itemize}
		\item The solution to the exponential growth equation here is
			\[
				Y(t) = C e^{r_0 t}.
			\]
		\item Using our information, we know first
			\[
				1 = Y(0) = C e^{r_0 \cdot 0} = C.
			\]
		\item Then
			\[
				2 = Y(1) = C e^{r_0 \cdot 1} = e^{r_0}.
			\]
		\item Solving for $r_0$ gives $r_0 = \ln 2$.
	\end{itemize}
	\item One thing to notice is that if we plug this into our equation, we get
		\[
			Y(t) = C e^{\ln(2) t} = C \left(e^{\ln(2)}\right)^t = C 2^t.
		\]
	\item So after each unit of time, the value of $Y$ gets multiplied by $2$, so it really is doubling each unit of time.
\end{itemize}

\section*{Exercise 3}

\begin{itemize}
	\item In this exercise, we're just solving this differential equation by separating variables and integrating.
	\item In this case, we get all of our $Z$ terms on one side, and just keep the $r_0$ term on the other side.
	\item To integrate the RHS, we just have to use hint given in the lab, that
		\[
			\frac{P}{(P - Z) Z} = \frac{1}{P - Z} - \frac{1}{Z},
		\]
		where each of these two terms are easier to integrate.
	\item Performing this integration, we get
		\[
			-\ln(P - Z) + \ln (Z) = r_0 t + C.
		\]
	\item Now we just use some algebra to solve for $t$ in terms of $Z$:
		\[
			t = \frac{\ln(Z) - \ln(P - Z) - C}{r_0}.
		\]
	\item Finally, we just solve for $C$.
		Plugging in $t = 1$ and $Z = 1$ gives
		\[
			1 = \frac{-\ln(P - 1) - C}{r_0},
		\]
		and therefore
		\[
			C = -\ln(P - 1) - r_0.
		\]
		So finally,
		\[
			t = \frac{\ln(Z) - \ln(P - Z) + \ln(P - 1) + r_0}{r_0}
		\]
		which we can simplify using properties of logarithms as
		\[
			t = \frac{\ln\left(\frac{Z(P - 1)}{P - Z}\right) + r_0}{r_0} 
		\]
	\item Now to check that everything came out right, we can just plug it into this box in the lab file and check to see that our plot matches (approximately) the experimental data.
\end{itemize}

\section*{Exercise 4}

\begin{itemize}
	\item Our last task in this lab to to figure out the rate that the decay in the remaining humans occurs at the end.
	\item If we look at a plot of $P - Z$, we see that it starts to taper off near the end here.
		In fact, it actually looks a lot like the start of the $Z$ graph, just decaying instead of growing.
	\item This would suggest that the rate of decay is actually the same as the rate of growth at the start, which we figured out was $r_0 = \ln(2)$ in Exercise 2.
	\item To try and further justify this, we can try starting with the information we're given
		\[
			P - Z \approx C e^{-rt}.
		\]
	\item Now, we could use our solution of the differential equation in Exercise 3 and try and solve for $Z$ to replace.
		But this is a little difficult and ugly.
	\item One common technique is to use the differential equation itself which is a lot simpler.
	\item If we differentiate, we get
		\[
			-Cre^{-rt} = \frac{d}{dt} (P - Z) = - \frac{dZ}{dt} = - \frac{r_0}{P} (P - Z) Z = - \frac{r_0}{P} Ce^{rt} Z.
		\]
	\item Dividing $-Ce^{rt}$ from both sides, we get
		\[
			r = r_0 \frac{Z}{P}.
		\]
	\item But remember that we're analyzing what happens as the number of zombies is almost equal to the population.
		So this means $Z \approx P$, or $Z / P \approx 1$.
	\item Thus, $r \approx r_0$, which matches our intuition.
\end{itemize}

\end{document}
