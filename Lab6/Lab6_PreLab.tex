\documentclass[12pt]{article}
\usepackage{fancyhdr}
\usepackage{geometry}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{enumitem}

\geometry{letterpaper, scale=0.80, hmarginratio={1:1}, vmarginratio={1:1}, headheight=20pt, headsep=0.2in, footskip=0.5in}

\pagestyle{fancy}
\fancyhf{}
\rhead{Feb. 26, 2019}
\lhead{MTH133}
\chead{\large \bf Lab 6: Pre-Lab}
\cfoot{\thepage}

\setlength\parindent{0pt}
\setlength\parskip{15pt}

\definecolor{grayBG}{gray}{0.85}
\newcommand\answerbox[1]{\raisebox{-0.8ex}{\textcolor{grayBG}{\rule{#1}{3ex}}}}

\begin{document}
\section*{Learning Goals}

Since the earliest implementations of mechanical calculators, to modern development of supercomputers, these devices are designed to be extremely efficient in performing the fundamental arithmetic operations of \textbf{addition}, \textbf{subtraction}, \textbf{multiplication}, and \textbf{division}.

Evaluations of other functions, however, are not built into the hardware, and must be replicated in software. 

Today's lab aims at answering the question: how does a computer/electronic calculator evaluate complicated functions like $\sin$, $\cos$, $\exp$ and $\ln$?
The answer resides in one of the highlights of Calculus II: the Taylor polynomial.

You will learn how \textbf{Taylor polynomials} allow us to efficiently create numerical approximations, as well as the central role played by the corresponding \textbf{radius/interval of convergence}.
Additionally, you will learn how to manipulate Taylor polynomials and use comparison tests in order to bound the error in a polynomial approximation to a function.

For concreteness, we will focus on the \textbf{natural logarithm} function $\ln$.

\section*{Option 1: A Look-up table}

Before the advent of modern electronic calculators, when one needs to use logarithms in practice, one consults a table of logarithms.
An excerpt of one such table is shown in Figure 1. The values shown in the table are pre-computed by hand, with the aid of a mechanical calculator. 

Such a table can be easily reproduced in a computer, especially since computers store numbers with finite \emph{precision} (that is, only using finitely many digits), meaning that there are only finitely many entries needed in our table.

\textbf{Except} that for modern uses, numbers are represented using 64 bits each.
A look-up table covering all valid 64 bit floating point numbers will, in principle, take storage space between 100 petabyte and 1 zetabyte (the latter being 1 billion terabytes).
Even for use with a simple 8-digit electronic calculator, a corresponding look-up table will take about 1 gigabyte of storage. 

So option 1 is not very practical. 

\newpage
\begin{figure}[ht!]
	\centering 
	\includegraphics[width=0.72\textwidth]{Figures/Logarithm_table.pdf}
	\label{Figure 1.}
	\caption{Logarithm table from Logarithmic and trigonometric tables by E.R. Hedrick. The Macmillan Company (1920).
	(Work in public domain, image extracted from archive.org.)}
\end{figure}


\section*{Option 2: Taylor polynomials}

A second option is to use Taylor polynomials. Remember from class that
\[
	\ln(1+x)=\sum_{n=1}^{\infty}(-1)^{n+1}\frac{x^n}{n}=x-\frac{x^2}{2}+\frac{x^3}{3}-\frac{x^4}{4}+\ldots,
\]
with radius of convergence $1$.

While it is not really feasible to sum the whole infinite series (which will require adding infinitely many numbers together), we can hope that the Maclaurin polynomial of sufficiently high degree will give a good enough approximation to the actual function for the values of $x$ where the series converges.

\section*{Bounding the remainder}

Since the radius of convergence of the aforementioned Macularin series is $1$, we know that the sum converges to the value of $\ln(1 + x)$ for any value of $x$ in the open interval $(-1, 1)$.
Additionally, it is possible that the series converges at the endpoints $-1$ and $1$, but this would need to be checked separately.

To stay away from the potentially dangerous endpoints, let's consider the difference between the $\ln(1 + x)$ and just a few terms of the Maclaurin series on a large chunk of the interval, $[-0.9, 0.9]$.
We'll first consider what happens when we use the fifth degree Maclaurin polynomial.

The difference between this fifth degree Maclaurin polynomial and the full Maclaurin series is

\[\sum_{n=1}^{\infty}\frac{(-1)^{n+1}x^n}{n}-\sum_{n=1}^5\frac{(-1)^{n+1}x^n}{n}=\sum_{n=6}^{\infty}\frac{(-1)^{n+1}x^n}{n}\]

Now, we only care about how big this difference is in magnitude, so we will take the absolute value.
However, the absolute value of an entire series can be difficult to work with.
Instead, we will make use of \textbf{absolute convergence} get an upper bound involving the absolute value of each term.

From \textbf{absolute convergence} we know that the absolute value of a series is bounded by its corresponding series of absolute values:

\[|\sum{a_{n}}|\leq\sum{|a_{n}|}\]

So we see that the magnitude of the difference between the 5th degree Maclaurin polynomial and the full Maclaurin series is at most

\[\sum_{n=6}^{\infty}\frac{|x|^n}{n}\]

But now this series doesn't help us out very much, since we'd like something that we can actually sum up, like a geometric series.
For this, we use the \textbf{direct comparison test}.

From the \textbf{direct comparison test}, we know that if two series $a_{n}$ and $b_{n}$ have positive terms, and $a_{n}<b_{n}$, then their sums satisfy

\[\sum{a_{n}}<\sum{b_{n}}\]

We can apply this to to our difference, and using that $|x|^n/n \leq x^n$, obtain that

\[\sum_{n=6}^{\infty}\frac{|x|^n}{n}<\sum_{n=6}^{\infty}|x|^n\]

So we reached our conclusion that the difference between the fifth degree Maclaurin polynomial for $\ln(1+x)$ and the full Maclaurin series is at most

\[\sum_{n=6}^{\infty}|x|^n\]

Now, this quantity is a geometric series, so we can sum it and see how large this sum can get on the interval $[-0.9, 0.9]$.
This will be the starting point for the lab, where you will use techniques like these to investigate efficient ways to calculate the specific value $\ln(2)$ with Taylor polynomials.  
\end{document}
