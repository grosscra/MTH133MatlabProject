\documentclass[12pt]{article}
\usepackage{fancyhdr}
\usepackage{geometry}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{enumitem}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{alltt}
\usepackage{array}

\geometry{letterpaper, scale=0.80, hmarginratio={1:1}, vmarginratio={1:1}, headheight=20pt, headsep=0.2in, footskip=0.5in}

\pagestyle{fancy}
\fancyhf{}
\rhead{Feb. 26, 2019}
\lhead{MTH133}
\chead{\large \bf Lab 7: Pre-Lab}
\cfoot{\thepage}

\setlength\parindent{0pt}
\setlength\parskip{15pt}

\definecolor{grayBG}{gray}{0.85}
\newcommand\answerbox[1]{\raisebox{-0.8ex}{\textcolor{grayBG}{\rule{#1}{3ex}}}}

\begin{document}

\section*{Learning Goals}
Most physical phenomena are described by \textbf{more than one} dependent variables.
In class we have just encountered the case of \textbf{parametrized curves} where to describe the position of an object, as a function of time, we need to simulaneously examine its x-coordinate and y-coordinate as a function of time. 

Today we will explore parametrized curves, and start you thinking about how to apply the calculus concepts of \textbf{integrals} and \textbf{derivatives} to these curves, ahead of the rigorous discussion we will give in class.
We will start our discussion with the help of the Etch A Sketch toy, and finish by analyzing the vehicle data from a 1963 Corvette Grand Sport on the Laguna Seca race track.

\section*{Etch a Sketch}
The Etch a Sketch is a toy invented in the 1960s. 
 
\begin{figure}[ht!]
\centering 
\includegraphics[scale = 1.5]{Figures/Taj_Mahal_drawing_on_an_Etch-A-Sketch.pdf}
\caption{The Etch A Sketch toy, with a drawing of Taj Mahal.
Image source \url{https://commons.wikimedia.org/wiki/File:Taj_Mahal_drawing_on_an_Etch-A-Sketch.jpg} used under CC-by-SA license.}
\end{figure}

The toy has two knobs.
Turning the left one moves a tracer up and down, and turning the right one moves a tracer left and right.
Mathematically, this means that the \textbf{vertical rate of change} of the tracer is given by the rate at which you twist the left knob, and the \textbf{horizontal rate of change} of the tracer is given by the rate at which you twist the right knob. 

The following graphic illustration will walk you through a stimulation of the Etch a Sketch toy using some simple \texttt{MATLAB} commands to prepare you for more complicated examples in the lab.

\subsection*{Left knob}

Let imagine the tracer starting at the point $(1,1)$, to plot this on MATLAB, we would use this line of code
\begin{center}
	\begin{tabular}{m{0.2\linewidth}c}
		\texttt{plot(1,1,'ro')} & 
		\begin{minipage}{0.6\textwidth}
			\includegraphics[width=\linewidth]{Figures/left_a-converted.pdf}
		\end{minipage}
	\end{tabular}
\end{center}
and as you can see, the tracer, a red circle, is now located at the point $(1,1)$.

Now we will turn the left knob steadily at 2 units per second.
We started at the point $(1, 1)$, but after turning the knob, the tracer moves up 2 units, so it ends at the point $(1, 3)$.
We plot the line and the tracer with the following code
\begin{center}
	\begin{tabular}{m{0.3\textwidth}c}
		\begin{minipage}{\linewidth}
			\texttt{plot([1,1], [1,3], 'r')}\\
			\texttt{plot(1, 3, 'ro')}
		\end{minipage} &
		\begin{minipage}{0.6\textwidth}
			\includegraphics[width=\linewidth]{Figures/left_b-converted.pdf}
		\end{minipage}
	\end{tabular}
\end{center}
The plot command first takes the list of $x$ values and then the list of $y$ values.

\subsection*{Right knob}

We now turn the right knob steadily at $-1$ unit per second, for a length of 1 second.
At the end of the last step, the tracer was at the point $(1, 3)$.
Next we turned the right knob, so the tracer moved to the left by 1 unit, ending at the point $(0,3)$.
We record all of our movement so far in the following line of code producing the corresponding trace on the Etch a Sketch.
\begin{center}
	\begin{tabular}{m{0.34\textwidth}c}
		\begin{minipage}{\linewidth}
			\texttt{plot([1,1,0], [1,3,3], 'r')}\\
			\texttt{plot(0, 3, 'ro')}
		\end{minipage} &
		\begin{minipage}{0.6\textwidth}
			\includegraphics[width=\linewidth]{Figures/right_a-converted.pdf}
		\end{minipage}
	\end{tabular}
\end{center}

\subsection*{Two knobs together}

We now turn the left knob and the right knob together, with the left knob going at -1 units per second, and the right knob moving at 2 units per second, for a duration of 1 second.

The left knob controls the $y$-coordinate, so moving at $-1$ units per second for 1 second will decrease the $y$-coordinate by 1. 
Starting from the point $(0,3)$, it will move to a point with $y$-coordinate 2. 
As the right knob controls the $x$-coordinate, moving it at 2 units per second for 1 second will increase the $x$-coordinate by 2.
Starting from $(0,3)$, it will move to a point with $x$-coordinate 2. 
To trace the path with the final coordinate as $(2, 2)$, we use the following code.
\begin{center}
	\begin{tabular}{m{0.37\textwidth}c}
		\begin{minipage}{\linewidth}
			\texttt{plot([1,1,0,2],[1,3,3,2],'r')}\\
			\texttt{plot(2, 2, 'ro')}
		\end{minipage} &
		\begin{minipage}{0.58\textwidth}
			\includegraphics[width=\linewidth]{Figures/right_b-converted.pdf}
		\end{minipage}
	\end{tabular}
\end{center}

\subsection*{How far has the tracer travelled?}

There are three steps to this process. 
\begin{itemize}
\item In the first step, the tracer moved up by 2 units. 
\item In the second step, the tracer moved sideways by 1 unit. 
\item In the final step, the tracer moved diagonally. It moved 2 units right and 1 unit down. The distance travelled in this step can be found using the Pythogorean theorem: the distance travelled is the length of the hypotenuse. So,
\[ \textrm{distance} = \sqrt{(\Delta{x})^2+(\Delta{y})^2}=\sqrt{1^2+2^2}=\sqrt{1+4}=\sqrt{5}\]
\end{itemize}

So in total, the distance travelled by the tracer is $2+1+\sqrt{5}=3+\sqrt{5}$ units.

\end{document}
