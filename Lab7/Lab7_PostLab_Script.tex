\documentclass[12pt]{article}
\usepackage{fancyhdr}
\usepackage{geometry}
\usepackage{graphicx}
\usepackage{enumitem}
\usepackage{amsmath}

\geometry{letterpaper, scale=0.80, hmarginratio={1:1}, vmarginratio={1:1}, headheight=20pt, headsep=0.2in, footskip=0.5in}

\pagestyle{fancy}
\fancyhf{}
\rhead{June 13, 2019}
\lhead{MTH133}
\chead{\large \bf Lab 7 Post-Lab Script}
\cfoot{\thepage}

\setlength\parindent{0pt}
\setlength\parskip{15pt}

\begin{document}

Note: This was originally written with an older version of the lab, so all $\Delta x_n$ or $\Delta y_n$ should be replaced by $ \frac{\Delta x_n}{\Delta t} $ or $ \frac{\Delta y_n}{\Delta t} $ respectively.


\section*{Exercise 1}

\begin{itemize}
		\item Welcome to Lab 7.
		\item Review setup.
		\begin{itemize}
			\item So here we're working with parameterized curves, where we have \emph{multiple} dependent variables.
			\item We're going to start small, with the idea of an etch-a-sketch.
			\item Remember that turning the left knob controls the up-and-down, or $y$-motion, and the right knob controls the left-and-right, or $x$-motion.
		\end{itemize}
		\item So let's try the setup in the lab document.
		\item \emph{(The following will be sketched on screen as it's discussed)}
		\begin{itemize}
			\item So first we're asked to turn the just the right knob at a speed of two units per second for two seconds.
				So in the first second, we'll go to the right two units, and then in the second second, we'll go another two units.
			\item Good, now we're asked to turn just the left knob at a speed of \emph{negative} one units per second for one second.
				So after one second, we'll go down one unit.
			\item Finally, we're asked to turn both knobs together.
				Turning the left knob at a rate of one unit per second for two seconds will move the tracer up two, and turning the right knob at a rate of negative one unit per second for two seconds will move it left two.
				So this step moves us up to the coordinate $(2, 2)$ in a straight line.
		\end{itemize}
		\item Finally, we want to figure out the distance traveled.
		\begin{itemize}
			\item The straight portions are pretty easy.
				The horizontal line is four units long, and the vertical line is one unit long.
			\item Now to get the distance of the diagonal line, we can use the Pythagorean theorem.
				We know it has to be the square root of the $x$-length squared plus the $y$-length squared which is $\sqrt{2^2 + 2^2} = \sqrt{8}$.
		\end{itemize}
\end{itemize}

\section*{Exercise 2}

\begin{itemize}
	\item Now, exercise 2 asks us to generalize this idea of getting from one point to another.
	\item If we know all of these approximate velocities moving from one point to the next, we can figure about where we are by multiplying that velocity with the amount of time we traveled at that velocity just as we did in exercise 1.
	\item \emph{(The next discussion will be made while drawing an example trace of the etch-a-sketch, labeling the quantities discussed)}
	\item So in this case, it looks something like $\Delta x_1 \times \frac{1}{10}$ for our new $x$-position, and $\Delta y_1 \times \frac{1}{10}$ for our new $y$-position.
	\item To go to the next position, we'll add on the information about the velocity given in the next time step, giving
	\[
		\left(\Delta x_1 \times \frac{1}{10} + \Delta x_2 \times \frac{1}{10}, \Delta y_1 \times \frac{1}{10}+ \Delta y_2 \times \frac{1}{10}\right).
	\]
	\item \emph{(We now write out this equation)}
	\item Continuing this process, we'll just add on the amount given by our new velocity each time, and since each is multiplied by $1/10$, we can factor it out, giving
	\[
		\left(\left(\Delta x_1 + \Delta x_2 + \Delta x_3 + \ldots + \Delta x_N\right) \times \frac{1}{10}, \left(\Delta y_1 + \Delta y_2 + \Delta y_3 + \ldots + \Delta y_N\right) \times \frac{1}{10}\right),
	\]
	or in sigma notation,
	\[
		\left(\left(\sum_{i = 1}^N \Delta x_i\right) \times \frac{1}{10}, \left(\sum_{i = 1}^N \Delta y_i\right) \times \frac{1}{10}\right).
	\]
	\item \emph{(We again label these quantities on the sketch and write out the equations.)}
	\item Now how much distance did we travel in each of these intervals?
	Well looking at the triangles in this sketch here, we can use the Pythagorean theorem to say the this first amount of distance traveled is
	\[
		\sqrt{\left(\frac{\Delta x_1}{10}\right)^2 + \left(\frac{\Delta y_1}{10}\right)^2},
	\]
	and the next is
	\[
		\sqrt{\left(\frac{\Delta x_2}{10}\right)^2 + \left(\frac{\Delta y_2}{10}\right)^2},
	\]
	and so on.
	\item So we just want to add all of these up to get the total distance traveled:
	\[
		\sqrt{\left(\frac{\Delta x_1}{10}\right)^2 + \left(\frac{\Delta y_1}{10}\right)^2} + \sqrt{\left(\frac{\Delta x_2}{10}\right)^2 + \left(\frac{\Delta y_2}{10}\right)^2} + \cdots \sqrt{\left(\frac{\Delta x_N}{10}\right)^2 + \left(\frac{\Delta y_N}{10}\right)^2}
	\]
	and in sigma notation,
	\[
		\sum_{i = 1}^N \sqrt{\left(\frac{\Delta x_i}{10}\right)^2 + \left(\frac{\Delta y_i}{10}\right)^2}.
	\]
	Notice again that we can factor out a $\frac{1}{10}$:
	\[
		\sum_{i = 1}^N \sqrt{\left(\Delta x_i\right)^2 + \left(\Delta y_i\right)^2} \times \frac{1}{10}.
	\]
\end{itemize}
		
\section*{Exercise 3}
\begin{itemize}
	\item Now remember what these $\Delta x_i$ and $\Delta y_i$ represent.
	They're \emph{approximations} of the velocities.
	\item So like our picture we used, we're saying that over this entire interval of $\frac{1}{10}$ of a second, we're assuming that our tracer is moving this same fixed velocity.
	However, it may not have been moving like this the entire time.
	For example, it could have started and ended moving very quickly, and based on our snapshots, that's what we would assume would happen the entire time.
	But it could be moving slowly in between, making a shorter line.
	\item Of course, if we take more and more measurements, our approximations we'll get better and better.
	We'll be able to snapshot more of these points and none of these weird irregularities will escape us.
	\item Let's take a look back at our equations and think about what this means.
	Remember that or snapshot length was $\frac{1}{10}$.
	If we're thinking about decreasing this length, let's write it as $\Delta t$:
	\[
		\left(\left(\sum_{i = 1}^N \Delta x_i\right) \times \Delta t, \left(\sum_{i = 1}^N \Delta y_i\right) \times \Delta t\right).
	\]
	\item \emph{(In the next steps, we scratch out the quantities in the discrete formula and replace them with their continuous counter parts)}
	\item As we take our $\Delta t$ closer and closer to zero, our approximations of the velocities, become the actual velocity, i.e.,
	\[
		\lim_{\Delta t \rightarrow 0} \Delta x_i = \frac{dx}{dt}, \qquad \lim_{\Delta t \rightarrow 0} \Delta y_i = \frac{dy}{dt}.
	\]
	\item And this sum, is really just a Riemann sum, with the $\Delta t$ the length of the base and the velocity the height.
	So as we take the limit, this becomes an integral:
	\[
		\lim_{\Delta t \rightarrow 0} \left(\sum_{i = 1}^N \Delta x_i\right) \times \Delta t = \int_0^{t_0} x'(t) \;d t, \qquad \lim_{\Delta t \rightarrow 0} \left(\sum_{i = 1}^N \Delta y_i\right) \times \Delta t = \int_0^{t_0} y'(t) \;d t.
	\]
	(where $t_0$ is the total amount of time that we consider).
	\item This should look familiar:
	\[
		\int_0^{t_0} \left(x'(t), y'(t)\right) \;d t
	\]
	is just the componentwise integration of the rates of change of $x(t)$ and $y(t)$ in our parameterization!
	And we know that when we integrate velocity, we get the position.
	\item So so far, everything checks out.
	\item Now let's do the same thing for our formula for distance traveled.
	\item \emph{(Again, scratch out the discrete parts and replace them.)}
	\item When we go back and switch out $\frac{1}{10}$ for $\Delta t$, it again looks just like a Riemann sum:
	\[
		\sum_{i = 1}^N \sqrt{\left(\Delta x_i\right)^2 + \left(\Delta y_i\right)^2} \times \Delta t.
	\]
	\item And by the same reasoning before, we can take the limit as our snapshots get smaller and smaller, and this becomes an integral:
	\[
		\lim_{\Delta t \rightarrow 0}\sum_{i = 1}^N \sqrt{\left(\Delta x_i\right)^2 + \left(\Delta y_i\right)^2} \times \Delta t = \int_0^{t_0} \sqrt{\left(x'(t)\right)^2 + \left(y'(t)\right)^2} \;d t.
	\]
	This is the exact formula for the distance traveled.
	\item If we think about this more generally, remember that the Etch a Sketch is really tracing out a curve on the screen.
	\item So if we have some parameterization for this curve $(x(t), y(t))$, the distance traveled by the Etch a Sketch is just the length of the curve.
	\item So this formula is the arc length of the curve parameterized by $(x(t), y(t))$.
\end{itemize}

\section*{Exercise 4}
\begin{itemize}
	\item So now we'd like to go back to this `snapshot' scenario and apply it to the real-life situation of the Corvette driving around the Laguna Seca track.
	\item \emph{(Scroll through the MATLAB file, highlighting the discussed items.)}
	\item After plugging in all of this data corresponding to the Corvette's $x$ and $y$ velocity, we can use our formula for the position that we found above to to figure out where on the track the Corvette is based on just its $x$ and $y$ velocities.
	\item This gives us a great plot of the curve traced out by the parameterization $(x(t), y(t))$ which is the path that the Corvette follows.
	By nudging this final value up a bit, we can trace out a little bit more of the curve in time, and figure out that the Corvette is traveling clockwise.
	\item \emph{(Have the final values already written down, and go back to the lab document)}
	\item This will also give us the times associated with when the Corvette traces out a complete lap, once we've increased our range so that our curve closes back in on itself which we have written down here.
	\item So we can now count out the laps, seeing that it gets around 7 full laps, almost reaching 8.
\end{itemize}

\section*{Exercise 5}
\begin{itemize}
	\item Now we would like to put all of these pieces together and figure out how long our track is.
	\item \emph{(Show previous formula for arc-length.)}
	\item Remember that we have a nice formula above for arc-length of a parameterized function.
	In this case, since we only have our snapshots, we're going to use the sum from above
	\[
		\sum_{i = 1}^N \sqrt{(\Delta x_i)^2 + (\Delta y_i)^2} \times \Delta t.
	\]
	\item Our snapshots are $\Delta t = \frac{1}{100}$ of a second apart, and our approximate velocities in the code are called $\texttt{vNorth}$ and $\texttt{vEast}$ respectively.
	\item \emph{(Write out the code in MATLAB.)}
	\item So in order to find the length traced out in the first $N$ time steps, we just want to calculate
	\[
		\sum_{i = 1}^N \sqrt{\texttt{vNorth}^2 + \texttt{vEast}^2} \times \frac{1}{100}.
	\]
	\item This gives us our function to sum.
	\item If we want to look over this first lap, that's the time steps from $1$ to $15800$.
	Plugging it in, we get, some number.
	\item Now let's look a little deeper at how this first lap compares to some of the other laps.
	\item As the lab talked about, over this first lap, the Corvette was originally stopped and had to get up to speed.
	So the Corvette traced out the path of the track much slower than it did on say the second lap.
	\item That's like saying if we parameterized the curve that the Corvette traced out on the first lap, it would be different than the parameterization of the curve on the second lap.
	\item The track is still the same, but the way the Corvette travels around it is different.
	\item Now, if our formula for arc-length or distance traveled is really correct, this \emph{shouldn't matter at all}.
	Let's give it a shot.
	\item \emph{(Copy the code and plug in the steps for the second lap.)}
	\item If we use the time steps corresponding to our second lap in the sum, we get something very similar.
	\item In fact, if we did this with each of the laps, we would get about the same length each time.
	\item So indeed, we really see that our formula for arc-length of the parameterized curve doesn't depend on the parameterization at all.
	\item \emph{(Show the Laguna Seca Wikipedia page.)}
	\item And to really check that we did things right, we searched the Laguna Seca on Wikipedia and found that it's actual length is 3.6 km which is very close to our estimation.
	Not too bad for calculating the length of a racetrack based on approximations of a car's speed!
\end{itemize}

\end{document}
